import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeeWithPositionModel } from 'src/app/models/EmployeeWithPositionsModel';
import { PositionModel } from 'src/app/models/PositionModel';
import { RoleModel } from 'src/app/models/RoleModel';
import { SelectedEmployeeTransferInfo } from 'src/app/SelectedEmployeeTransferInfo';
import { AdminService } from 'src/app/services/admin.service';
import { GetClickedEmployeeIdService } from 'src/app/services/get-clicked-employee-id.service';
import { showSuccessMessage, hideSuccessMessage } from 'src/app/successMessageFunctions';
import { showLoadingBar, hideLoadingBar } from 'src/app/loadingBarFunctions';

@Component({
  selector: 'app-change-roles',
  templateUrl: './change-roles.component.html',
  styleUrls: ['./change-roles.component.css', '../../shared/styles/find-employees.css']
})
export class ChangeRolesComponent implements OnInit {

  findEmployeesForm : FormGroup = new FormGroup({
    firstName : new FormControl("",Validators.required),
    lastName : new FormControl("",Validators.required)
  });

  findPositionForm : FormGroup = new FormGroup({
    findPosition : new FormControl("",Validators.required)
  });

  isEmployeeRole : boolean = false;

  projectId : number;
  selectedEmployeeId : number = Number.NaN;
  selectedPositionId : number | null = null;
  selectedRoleId : number = Number.NaN;

  positions : PositionModel[];
  roles : RoleModel[];
  employees : EmployeeWithPositionModel[];

  selectedEmployeeInfo : SelectedEmployeeTransferInfo = new SelectedEmployeeTransferInfo();

  constructor(private adminService : AdminService, private router : Router, private getSelectedEmployeeService : GetClickedEmployeeIdService) 
  {
    this.projectId = +(this.router.getCurrentNavigation()?.extras.state as any).projectId;
  }

  ngOnInit(): void {
    this.findRoles();
  }

  findEmployees(){
    if(this.findEmployeesForm.invalid){
      this.findEmployeesForm.markAllAsTouched();
      return;
    }
    let firstName = this.findEmployeesForm.controls['firstName'].value;
    let lastName = this.findEmployeesForm.controls['lastName'].value;

    this.adminService.getEmployeesByName(firstName,lastName,this.projectId).subscribe(resp=>this.employees=resp);
  }

  findRoles(){
    this.adminService.findRoles(this.projectId).subscribe(resp=>this.roles=resp);
  }

  onRoleSelect(){
    let element = document.getElementById('selectRole') as any;
    let selectedRole = element.options[element.selectedIndex].text;
    this.selectedRoleId = element.value;
    if(selectedRole == 'Employee')
      this.isEmployeeRole = true;
    else
      this.isEmployeeRole = false;
  }

  findPositions(){
    if(this.findPositionForm.invalid){
      this.findPositionForm.markAllAsTouched();
      return;
    }
    (document.getElementsByClassName('found-positions')[0] as HTMLElement).style.display='inline-flex';
    let positionName = this.findPositionForm.controls['findPosition'].value;
    this.adminService.findPositions(positionName, this.projectId).subscribe(resp=>this.positions=resp);
  }

  getEmployeeId(event : Event){
    /*let target = event.target as Element;
    let parent = target.parentElement as HTMLElement;
    if(parent.attributes.getNamedItem('id')!=null){
        let container = document.getElementsByClassName('modal-inner')[0] as HTMLElement;
        this.selectedEmployeeId = +(parent.attributes.getNamedItem("id") as Attr).value;
    }*/

    this.getSelectedEmployeeService.getClickedEmployeeId(event, this.selectedEmployeeInfo);
    this.selectedEmployeeId = this.selectedEmployeeInfo.selectedEmployeeId;

  }

  submitChanges(){
    this.hideCustomErrorMessages();
    hideSuccessMessage();

    if(this.findEmployeesForm.invalid){
      this.findEmployeesForm.markAllAsTouched();
      return;
    }

    if(Number.isNaN(this.selectedEmployeeId) || this.selectedEmployeeId === undefined){
      (document.getElementById('no-selected-employee-error') as HTMLElement).style.display = 'block';
      return;
    }
    if(Number.isNaN(this.selectedRoleId) || this.selectedRoleId === undefined){
      (document.getElementById('no-selected-role-error') as HTMLElement).style.display = 'block';
      return;
    }

    let selectElementWithPositions = document.getElementById('selectPosition') as any;

    if(this.isEmployeeRole){

      if(this.findPositionForm.invalid){
        this.findPositionForm.markAllAsTouched();
        return;
      }

      if(selectElementWithPositions.value == ''){
        (document.getElementById('no-selected-position-error') as HTMLElement).style.display = 'block';
        return;  
      }
      this.selectedPositionId = selectElementWithPositions.value;
      showLoadingBar();

    this.adminService.setRoleAsEmployee(this.projectId,this.selectedEmployeeId,this.selectedRoleId,this.selectedPositionId).subscribe(resp=>{hideLoadingBar();showSuccessMessage(); },error=>{hideLoadingBar(); console.log(error.error)});

    }
    else{
    this.selectedPositionId = null;
      showLoadingBar();
    this.adminService.setSpecialRole(this.projectId,this.selectedEmployeeId,this.selectedRoleId).subscribe(resp=>{hideLoadingBar(); showSuccessMessage();},error=>{console.log(error.error); hideLoadingBar();});

    }
  }

  hideCustomErrorMessages(){
    (document.getElementById('no-selected-position-error') as HTMLElement).style.display = 'none';
    (document.getElementById('no-selected-role-error') as HTMLElement).style.display = 'none';
    (document.getElementById('no-selected-employee-error') as HTMLElement).style.display = 'none';

  }
}
