import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { ProjectCreationModel } from 'src/app/models/ProjectCreationModel';
import { AdminService } from 'src/app/services/admin.service';
import { checkDateForProjectStart } from 'src/app/validators/dateValidator';
import { showSuccessMessage, hideSuccessMessage } from '../../successMessageFunctions';
import { showLoadingBar, hideLoadingBar } from 'src/app/loadingBarFunctions';

@Component({
  selector: 'app-add-new-project',
  templateUrl: './add-new-project.component.html',
  styleUrls: ['./add-new-project.component.css']
})
export class AddNewProjectComponent implements OnInit {

  addProjectForm = new FormGroup({
    projectName : new FormControl("",Validators.required),
    projectDescription : new FormControl("",Validators.required),
    startDate : new FormControl("", [Validators.required,checkDateForProjectStart()])
  });
  constructor(private adminService : AdminService) { }

  ngOnInit(): void {
  }

  submitProject(){
    showLoadingBar();
    if(this.addProjectForm.invalid){
      this.addProjectForm.markAllAsTouched();
      console.log(this.addProjectForm.controls['startDate'].value);
      return;
    }
    hideSuccessMessage();
    let project = this.getProjectModelFromForm();

    this.adminService.addNewProject(project).subscribe(r=>{showSuccessMessage(); hideLoadingBar();},error=>{console.log(error.error); hideLoadingBar();});
  }

  getProjectModelFromForm() : ProjectCreationModel{
    let projectModel : ProjectCreationModel = new ProjectCreationModel();

    projectModel.projectName = this.addProjectForm.controls['projectName'].value;
    projectModel.projectDescription = this.addProjectForm.controls['projectDescription'].value;
    projectModel.startDate = this.addProjectForm.controls['startDate'].value;

    return projectModel;


  }

}
