import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeWithPositionCardComponent } from './employee-with-position-card.component';

describe('EmployeeWithPositionCardComponent', () => {
  let component: EmployeeWithPositionCardComponent;
  let fixture: ComponentFixture<EmployeeWithPositionCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeWithPositionCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeWithPositionCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
