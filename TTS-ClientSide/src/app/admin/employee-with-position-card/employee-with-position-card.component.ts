import { Component, Input, OnInit } from '@angular/core';
import { EmployeeWithPositionModel } from 'src/app/models/EmployeeWithPositionsModel';

@Component({
  selector: 'app-employee-with-position-card',
  templateUrl: './employee-with-position-card.component.html',
  styleUrls: ['./employee-with-position-card.component.css', '../../shared/styles/employee-card.css']
})
export class EmployeeWithPositionCardComponent implements OnInit {

  @Input() employee : EmployeeWithPositionModel;

  constructor() { }

  ngOnInit(): void {
  }

}
