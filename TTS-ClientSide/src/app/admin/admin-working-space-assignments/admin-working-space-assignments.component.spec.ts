import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkingSpaceAssignmentsComponent } from './admin-working-space-assignments.component';

describe('AdminWorkingSpaceAssignmentsComponent', () => {
  let component: AdminWorkingSpaceAssignmentsComponent;
  let fixture: ComponentFixture<AdminWorkingSpaceAssignmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminWorkingSpaceAssignmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkingSpaceAssignmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
