import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectCardModel } from 'src/app/models/ProjectCardModel';
import { ProjectWithTasksModel } from 'src/app/models/ProjectWithTasksModel';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-admin-working-space-assignments',
  templateUrl: './admin-working-space-assignments.component.html',
  styleUrls: ['./admin-working-space-assignments.component.css', '../../shared/styles/working-space-assignments.css']
})
export class AdminWorkingSpaceAssignmentsComponent implements OnInit {

  @Input() projectWithTasks : ProjectWithTasksModel;

  constructor(private adminService : AdminService, private router : ActivatedRoute) { }

  ngOnInit(): void {
    let projId = +this.router.snapshot.paramMap.get("projectId")!;
    let stringPageNum = this.router.snapshot.paramMap.get("taskPageNum");
    let taskPageNum = stringPageNum === null ? 0 : +stringPageNum;
    this.adminService.getProject(projId,taskPageNum).subscribe(resp=>this.projectWithTasks=resp);

  }

}
