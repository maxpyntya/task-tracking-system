import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmProjectDeletionModalComponent } from './confirm-project-deletion-modal.component';

describe('ConfirmProjectDeletionModalComponent', () => {
  let component: ConfirmProjectDeletionModalComponent;
  let fixture: ComponentFixture<ConfirmProjectDeletionModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmProjectDeletionModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmProjectDeletionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
