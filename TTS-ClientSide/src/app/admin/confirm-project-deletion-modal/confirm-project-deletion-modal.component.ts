import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProjectCardModel } from 'src/app/models/ProjectCardModel';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-confirm-project-deletion-modal',
  templateUrl: './confirm-project-deletion-modal.component.html',
  styleUrls: ['./confirm-project-deletion-modal.component.css']
})
export class ConfirmProjectDeletionModalComponent implements OnInit {

  constructor(private adminService : AdminService) { }

  @Input() project : ProjectCardModel;
  @Output() closeModal : EventEmitter<any> = new EventEmitter();

  ngOnInit(): void {
  }

  deleteProject(){
    this.adminService.deleteProject(this.project.projectId).subscribe(r=>{location.reload();},error=>{console.log(error.error)});
  }
  
  closeThisModal(event : Event){
    if(event.target == document.getElementsByClassName('modal-outer')[0])
      this.closeModal.emit();
  }

  closeModalWithButton(){
    this.closeModal.emit();

  }
}
