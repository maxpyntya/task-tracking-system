import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProjectCardModel } from 'src/app/models/ProjectCardModel';

@Component({
  selector: 'app-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.css']
})
export class ProjectCardComponent implements OnInit {

  @Output() selectProject : EventEmitter<any> = new EventEmitter();

  @Input() project : ProjectCardModel = new ProjectCardModel();
  @Input() userRole : string;
  constructor() { }

  ngOnInit(): void {
  }

  returnProjectForDeletion(){
    this.selectProject.emit({data : this.project});
  }

}
