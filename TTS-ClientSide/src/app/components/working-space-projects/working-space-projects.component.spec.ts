import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkingSpaceProjectsComponent } from './working-space-projects.component';

describe('WorkingSpaceProjectsComponent', () => {
  let component: WorkingSpaceProjectsComponent;
  let fixture: ComponentFixture<WorkingSpaceProjectsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkingSpaceProjectsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkingSpaceProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
