import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/UserService';
import { ProjectCardModel } from 'src/app/models/ProjectCardModel';
import { EmployeeService } from 'src/app/services/employee.service';
import { SpecifyRoleService } from 'src/app/services/specify-role.service';
import { ProjectCardComponent } from '../project-card/project-card.component';
import { UserServiceFactoryService } from 'src/app/services/user-service-factory.service';

@Component({
  selector: 'app-working-space-projects',
  templateUrl: './working-space-projects.component.html',
  styleUrls: ['./working-space-projects.component.css']
})
export class WorkingSpaceProjectsComponent implements OnInit {

  projects : ProjectCardModel[];
  userService : UserService;
  currentUserRole : string;
  availableUserRoles : string;
  selectedProjectForDeletion : ProjectCardModel;

  constructor(private router : Router, private userServiceFactory : UserServiceFactoryService, private roleService : SpecifyRoleService) { }

  ngOnInit(): void {
    this.currentUserRole = this.router.url.split('/')[1];
    this.availableUserRoles = this.roleService.getAllRoles();
    this.userService = (this.userServiceFactory.getAccordingService(this.currentUserRole))();
    this.userService.getProjects().subscribe(resp=>this.projects = resp);
  }

  getProjectForDeletion(event : any){
    this.selectedProjectForDeletion = event.data as ProjectCardModel;
    let modal = document.getElementsByClassName('modal-outer')[0] as HTMLElement;
    modal.style.display = 'block';
  }

  closeConfirmDeletionModal(){
        let modal = document.getElementsByClassName('modal-outer')[0] as HTMLElement;
        modal.style.display = 'none';


  }

}
