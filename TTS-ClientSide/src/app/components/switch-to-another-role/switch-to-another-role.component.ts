import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-switch-to-another-role',
  templateUrl: './switch-to-another-role.component.html',
  styleUrls: ['./switch-to-another-role.component.css']
})
export class SwitchToAnotherRoleComponent implements OnInit {

  @Input() allUserRoles : string;
  @Input() currentUserRole : string;

  rolesAvailableForSwitch : string[];

  showRolesToSwitch : boolean = false;

  constructor() { }

  ngOnInit(): void {
    let allRolesAvailableArray = this.allUserRoles.toString().toLowerCase().split(',');
    let indexOfCurrentRole = allRolesAvailableArray.indexOf(this.currentUserRole);
    allRolesAvailableArray.splice(indexOfCurrentRole,1);

    this.rolesAvailableForSwitch = allRolesAvailableArray;
  }

  toggleShowRolesToSwitch(){
    this.showRolesToSwitch = !this.showRolesToSwitch;
  }

}
