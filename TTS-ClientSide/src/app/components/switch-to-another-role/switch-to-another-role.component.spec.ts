import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchToAnotherRoleComponent } from './switch-to-another-role.component';

describe('SwitchToAnotherRoleComponent', () => {
  let component: SwitchToAnotherRoleComponent;
  let fixture: ComponentFixture<SwitchToAnotherRoleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwitchToAnotherRoleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchToAnotherRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
