import { AfterContentInit, Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AssignmentModel } from 'src/app/models/AssignmentModel';
import { AssignmentClassDefinerService } from 'src/app/services/assignment-class-definer.service';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html',
  styleUrls: ['./assignment.component.css', '../../shared/styles/assignment-status.css']
})
export class AssignmentComponent implements OnInit, AfterContentInit {


  @Input() assignment : AssignmentModel;

  @Input() originalStatus : string = "";

  assignmentClasses : any;
  assignmentChanged : boolean = false;

  statusUpdateForm : FormGroup = new FormGroup({
    newStatus : new FormControl("")
  });

  constructor(private empService : EmployeeService, public assignmentClassDefiner : AssignmentClassDefinerService) {
    this.assignmentClasses = assignmentClassDefiner.classes;
    let a = assignmentClassDefiner.getClass('Done');
   }

  ngAfterContentInit(): void {
    this.statusUpdateForm.controls['newStatus'].setValue(this.assignment.assignmentStatus);
    this.originalStatus = this.assignment.assignmentStatus;
  }

  ngOnInit(): void {
  }

  submit(){
    this.empService.updateTaskStatus(this.assignment.assignmentId,this.statusUpdateForm.controls['newStatus'].value).subscribe();
    window.location.reload();
  }

  assignOriginalStatus(){
    if(this.statusUpdateForm.controls['newStatus'].value===''){
      this.statusUpdateForm.controls['newStatus'].setValue(this.assignment.assignmentStatus);
      this.assignmentChanged = false;
    }
    else if(this.statusUpdateForm.controls['newStatus'].value===this.originalStatus)
      this.assignmentChanged = false;

  }

  clearStatusField(){
    this.statusUpdateForm.controls['newStatus'].setValue("");
    this.assignmentChanged = true;
  }

}
