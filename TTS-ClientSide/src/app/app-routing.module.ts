import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { EmployeeWorkingSpaceAssignmentsComponent } from './employee/employee-working-space-assignments/employee-working-space-assignments.component';
import { WorkingSpaceProjectsComponent } from './components/working-space-projects/working-space-projects.component';
import { EmployeeComponent } from './employee/employee.component';
import { RoleRedirectGuard } from './guards/roleRedirect/role-redirect.guard';
import { ManagerComponent } from './manager/manager.component';
import { ManagerWorkingSpaceAssignmentsComponent } from './manager/manager-working-space-assignments/manager-working-space-assignments.component';
import { IssueNewTaskComponent } from './manager/issue-new-task/issue-new-task.component';
import { AdminComponent } from './admin/admin.component';
import { AdminWorkingSpaceAssignmentsComponent } from './admin/admin-working-space-assignments/admin-working-space-assignments.component';
import { ChangeRolesComponent } from './admin/change-roles/change-roles.component';
import { AddNewProjectComponent } from './admin/add-new-project/add-new-project.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';


const routes: Routes = [
  {path: "", redirectTo:"login", pathMatch:"full"},
  {path:"login", component : LoginComponent},
  {path: "register", component : RegisterComponent},
  {path: "employee", component: EmployeeComponent, canActivate : [RoleRedirectGuard], data:{expectedRole : "Employee"}, children:[
    {path:"projects", component:WorkingSpaceProjectsComponent},
    {path:"proj/:projectId/:taskPageNum?", component: EmployeeWorkingSpaceAssignmentsComponent, pathMatch: 'full'}
  ]},
  {path: "manager", component: ManagerComponent, canActivate : [RoleRedirectGuard], data:{expectedRole: "Manager"}, children : [
    {path: "projects", component: WorkingSpaceProjectsComponent},
    {path : "proj/:projectId/:taskPageNum?", component: ManagerWorkingSpaceAssignmentsComponent, pathMatch: 'full'},
    {path : "proj/newTask", component: IssueNewTaskComponent}
  ]},

  {path: "admin", component : AdminComponent, canActivate: [RoleRedirectGuard], data: {expectedRole : "Admin"}, children : [
    {path: "projects", component: WorkingSpaceProjectsComponent},
    {path: "proj/:projectId/:taskPageNum?", component : AdminWorkingSpaceAssignmentsComponent, pathMatch: 'full'},
    {path: "proj/changeRoles", component: ChangeRolesComponent},
    {path: "newProject", component : AddNewProjectComponent}
  ]},

  {path: '404', component: PageNotFoundComponent},
  {path: '**', redirectTo: '/404'}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
