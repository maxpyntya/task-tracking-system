import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";


@Injectable()
export class TTSInterceptor implements HttpInterceptor{
    private router : Router;
    /**
     *
     */
    constructor(router : Router) {
        this.router = router;        
    }
    intercept(req: HttpRequest<any>, next : HttpHandler) : Observable<HttpEvent<any>>{
        let token = localStorage.getItem("Token");
        if(token === null || token === "")
            return next.handle(req);
        let clonedRequest = req.clone({headers : req.headers.set("Authorization","Bearer "+token)});
        return next.handle(clonedRequest).pipe(catchError(err=>{
            if(err.status === 401)
                this.router.navigate(['/login']);

            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
}