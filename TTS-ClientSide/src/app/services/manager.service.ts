import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AssignmentModel } from '../models/AssignmentModel';
import { EmployeeModel } from '../models/EmployeeModel';
import { PositionModel } from '../models/PositionModel';
import { ProjectCardModel } from '../models/ProjectCardModel';
import { ProjectWithTasksModel } from '../models/ProjectWithTasksModel';
import { UserService } from '../UserService';

@Injectable({
  providedIn: 'root'
})
export class ManagerService implements UserService{
  private getProjectsUrl = "https://localhost:44393/api/Manager/Projects";
  private getProjectUrl = "https://localhost:44393/api/Manager/Project";
  private getEmployeesByName = "https://localhost:44393/api/Manager/Employees";
  private assignEmployeeToProjectUrl = "https://localhost:44393/api/Manager/AssignProject";
  private getPositionsByNameUrl = "https://localhost:44393/api/Manager/Positions";
  private findEmployeesOnProject = "https://localhost:44393/api/Manager/PresentEmployees";
  private issueTaskToEmployeeUrl = "https://localhost:44393/api/Manager/Task";

  constructor(private httpClient : HttpClient) { }

  getProject(projectId: number, tasksPageNum: number): Observable<ProjectWithTasksModel> {
    return this.httpClient.get<ProjectWithTasksModel>(this.getProjectUrl+"/"+projectId+"/"+tasksPageNum);
  }

  getProjects() : Observable<ProjectCardModel[]>{
    return this.httpClient.get<ProjectCardModel[]>(this.getProjectsUrl);
  }

  findEmployeesByNameToAssignProject(firstName : string, lastName : string, projectId : number) : Observable<EmployeeModel[]>{
    return this.httpClient.get<EmployeeModel[]>(this.getEmployeesByName+"/"+projectId+"?firstName="+firstName+"&lastName="+lastName);
  }

  findEmployeesByNameOnProject(firstName : string, lastName : string, projectId : number) : Observable<EmployeeModel[]>{
    return this.httpClient.get<EmployeeModel[]>(this.findEmployeesOnProject+"/"+projectId+"?firstName="+firstName+"&lastName="+lastName);
  }

  findPositionsByName(position : string) : Observable<PositionModel[]>{
    return this.httpClient.get<PositionModel[]>(this.getPositionsByNameUrl+"?position="+position);
  }

  assignEmployeeToProject(projectId : number, employeeId : number, positionId : number){
    return this.httpClient.post(this.assignEmployeeToProjectUrl+"/"+projectId+"/"+employeeId+"/"+positionId,"");
  }

  issueTaskToEmployee(empoyeeId : number, assignment : AssignmentModel){
    return this.httpClient.post(this.issueTaskToEmployeeUrl+"/"+empoyeeId,assignment);
  }
}
