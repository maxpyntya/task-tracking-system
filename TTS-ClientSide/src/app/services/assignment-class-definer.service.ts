import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AssignmentClassDefinerService {

  public readonly classes : any =
   {'Done' : 'assignment-done',
   'In progress' : 'assignment-in-progress',
   'No Employee' : 'assignment-no-employee',
   'New' : 'assignment-new'
  };

  getClass(toFind : string) : string{
    return this.classes[toFind];
  }

  constructor() { }
}
