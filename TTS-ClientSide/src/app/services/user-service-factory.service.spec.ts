import { TestBed } from '@angular/core/testing';

import { UserServiceFactoryService } from './user-service-factory.service';

describe('UserServiceFactoryService', () => {
  let service: UserServiceFactoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserServiceFactoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
