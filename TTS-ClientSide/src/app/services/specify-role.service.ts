import { Injectable } from '@angular/core';
import decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class SpecifyRoleService {

  constructor() { }

  hasRole(expectedRole : string) : boolean{
    const token = localStorage.getItem("Token")+"";
    let payload : any = decode(token);
    let rolesInJWT : string = payload.role;
    if(rolesInJWT.includes(expectedRole))
      return true;

    return false;

  }

  getAllRoles() : string{
    const token = localStorage.getItem("Token")+"";
    let payload : any = decode(token);
    let rolesInJWT : string = payload.role;

    return rolesInJWT;
  }
}
