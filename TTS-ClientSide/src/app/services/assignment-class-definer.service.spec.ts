import { TestBed } from '@angular/core/testing';

import { AssignmentClassDefinerService } from './assignment-class-definer.service';

describe('AssignmentClassDefinerService', () => {
  let service: AssignmentClassDefinerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AssignmentClassDefinerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
