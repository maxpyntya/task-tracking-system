import { TestBed } from '@angular/core/testing';

import { GetClickedEmployeeIdService } from './get-clicked-employee-id.service';

describe('GetClickedEmployeeIdService', () => {
  let service: GetClickedEmployeeIdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetClickedEmployeeIdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
