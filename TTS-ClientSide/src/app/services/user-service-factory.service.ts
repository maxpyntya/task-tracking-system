import { HttpClient, HttpHandler } from '@angular/common/http';
import { Injectable, Type } from '@angular/core';
import { AdminService } from './admin.service';
import { EmployeeService } from './employee.service';
import { ManagerService } from './manager.service';

@Injectable({
  providedIn: 'root'
})
export class UserServiceFactoryService {

  constructor(private httpClient : HttpClient) { }

  private roleServices : any = {
    "employee" : ()=>new EmployeeService(this.httpClient),
    "manager" : ()=>new ManagerService(this.httpClient),
    "admin" : ()=>new AdminService(this.httpClient)
  }

  getAccordingService(role : string) : Function{
    let serviceType  = this.roleServices[role];
    return serviceType;
  }
}
