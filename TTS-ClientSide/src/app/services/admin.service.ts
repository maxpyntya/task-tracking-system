import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmployeeWithPositionModel } from '../models/EmployeeWithPositionsModel';
import { PositionModel } from '../models/PositionModel';
import { ProjectCardModel } from '../models/ProjectCardModel';
import { ProjectCreationModel } from '../models/ProjectCreationModel';
import { ProjectWithTasksModel } from '../models/ProjectWithTasksModel';
import { RoleModel } from '../models/RoleModel';
import { UserService } from '../UserService';

@Injectable({
  providedIn: 'root'
})
export class AdminService implements UserService {

  private getProjectsUrl = "https://localhost:44393/api/Administrator/Projects";
  private getProjectUrl = "https://localhost:44393/api/Administrator/Project";
  private getEmployeesByNameUrl = "https://localhost:44393/api/Administrator/Employees";
  private getRolesByNameUrl = "https://localhost:44393/api/Administrator/Project";
  private getPositionsByNameUrl = "https://localhost:44393/api/Administrator/Project";
  private setRolesUrl = "https://localhost:44393/api/Administrator/Project/SetRole";
  private addNewProjectUrl = "https://localhost:44393/api/Administrator/Project/New";
  private deleteProjectUrl = "https://localhost:44393/api/Administrator/Project/Delete"

  constructor(private httpClient : HttpClient) { }

  getProjects(): Observable<ProjectCardModel[]> {
    return this.httpClient.get<ProjectCardModel[]>(this.getProjectsUrl);
  }
  getProject(projectId: number, tasksPageNum: number): Observable<ProjectWithTasksModel> {
    return this.httpClient.get<ProjectWithTasksModel>(this.getProjectUrl+"/"+projectId+"/"+tasksPageNum);
  }

  getEmployeesByName(firstName : string, lastName : string, projectId : number) : Observable<EmployeeWithPositionModel[]>{
    return this.httpClient.get<EmployeeWithPositionModel[]>(this.getEmployeesByNameUrl+"/"+projectId+"?firstName="+firstName+"&lastName="+lastName);
  }

  findRoles(projectId : number, ) : Observable<RoleModel[]>{
    return this.httpClient.get<RoleModel[]>(this.getRolesByNameUrl+"/"+projectId+"/Roles");
  }

  findPositions(positionName : string, projectId : number) : Observable<PositionModel[]>{
    return this.httpClient.get<PositionModel[]>(this.getPositionsByNameUrl+"/"+projectId+"/Positions"+"?positionToFind="+positionName);
  }

  setRoleAsEmployee(projectId : number, employeeId : number, roleId : number, positionId : number | null){
    return this.httpClient.post(this.setRolesUrl+"/"+projectId+"/"+employeeId+"/"+roleId+"/"+positionId,"");
  }

  setSpecialRole(projectId : number, employeeId : number, roleId : number){
    return this.httpClient.post(this.setRolesUrl+"/"+projectId+"/"+employeeId+"/"+roleId,"");
  }

  addNewProject(project : ProjectCreationModel){
    return this.httpClient.post(this.addNewProjectUrl,project);
  }

  deleteProject(projectId : number){
    return this.httpClient.delete(this.deleteProjectUrl+"/"+projectId);
  }

}
