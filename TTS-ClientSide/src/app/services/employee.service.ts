import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProjectCardComponent } from '../components/project-card/project-card.component';
import { UserService } from '../UserService';
import { ProjectCardModel } from '../models/ProjectCardModel';
import { ProjectWithTasksModel } from '../models/ProjectWithTasksModel';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService implements UserService {
  private showProjectsUrl : string = "https://localhost:44393/api/Employee/Projects";
  private getProjectUrl : string = "https://localhost:44393/api/Employee/Project/";
  private updateTaskStatusUrl : string = "https://localhost:44393/api/Employee/Task"

  constructor(private httpClient : HttpClient) { }

  private headers : HttpHeaders = new HttpHeaders({
    "Content-Type" : "text/plain"
  });

  getProjects() : Observable<ProjectCardModel[]>{
    return this.httpClient.get<ProjectCardModel[]>(this.showProjectsUrl);
  }

  getProject(projectId : number,tasksPageNum : number) : Observable<ProjectWithTasksModel>{
    return this.httpClient.get<ProjectWithTasksModel>(this.getProjectUrl+projectId+"/"+tasksPageNum);
  }

  updateTaskStatus(taskId : number, newTaskStatus : string) : Observable<ProjectWithTasksModel>{
    return this.httpClient.put<ProjectWithTasksModel>(this.updateTaskStatusUrl+"/"+taskId+"?newStatus="+newTaskStatus,"");
  }
}
