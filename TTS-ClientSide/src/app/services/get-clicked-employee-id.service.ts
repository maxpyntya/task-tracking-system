import { Injectable } from '@angular/core';
import { SelectedEmployeeTransferInfo } from '../SelectedEmployeeTransferInfo';

@Injectable({
  providedIn: 'root'
})
export class GetClickedEmployeeIdService {

  constructor() { }

  getClickedEmployeeId(event : Event, selectedEmployeeInfo : SelectedEmployeeTransferInfo){
    let target = this.getClickedCard(event);
    //let target = target as HTMLElement;
    let aa = (target.attributes.getNamedItem('id') as Attr).value;
    if(target.attributes.getNamedItem('id')!=null && Number.isInteger(+(target.attributes.getNamedItem('id') as Attr).value)){
        if(selectedEmployeeInfo.previousClickedEmployee != undefined)
        selectedEmployeeInfo.previousClickedEmployee.classList.remove('employee-card-clicked');

        let container = document.getElementsByClassName('modal-inner')[0] as HTMLElement;
        selectedEmployeeInfo.selectedEmployeeId = +(target.attributes.getNamedItem("id") as Attr).value;
        selectedEmployeeInfo.employeeSelected = true;
        
        target.classList.add('employee-card-clicked');
        selectedEmployeeInfo.previousClickedEmployee = target;
    }

  }

  getClickedCard(event : Event) : HTMLElement{
    let target = event.target as Element;
    if(target.attributes.getNamedItem('id')!=null && Number.isInteger(+(target.attributes.getNamedItem('id') as Attr).value))
      return target as HTMLElement;

    return target.parentElement as  HTMLElement;
  }

}
