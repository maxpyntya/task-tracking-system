import { TestBed } from '@angular/core/testing';

import { SpecifyRoleService } from './specify-role.service';

describe('SpecifyRoleService', () => {
  let service: SpecifyRoleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpecifyRoleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
