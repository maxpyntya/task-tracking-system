export class ProjectCreationModel{
    projectName : string;
    projectDescription : string;
    percentageOfCompletion : number;
    startDate : Date;

    constructor() {
        this.projectName = "";
        this.projectDescription = "";
        this.percentageOfCompletion = 0;
        this.startDate = new Date();
    }
}