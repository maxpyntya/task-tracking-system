import { AssignmentModel } from "./AssignmentModel";

export class ProjectWithTasksModel{
    projectId : number;
    projectName : string;
    projectDescription : string;
    startDate : Date;
    closeDate? : Date;
    percentageOfCompletion : number;
    assignments : AssignmentModel[];

    constructor(){
        this.projectId =0;
        this.projectName = "";
        this.projectDescription = "";
        this.startDate = new Date();
        this.percentageOfCompletion = 0;
        this.assignments = [];
    }
}