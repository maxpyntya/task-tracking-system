export class EmployeeWithPositionModel{
    id : number;
    firstName : string;
    lastName : string;
    username : string;
    role : string;
    position: string;
    email : string;

    constructor(){
        this.id = 0;
        this.firstName = "";
        this.lastName = "";
        this.username = "";
        this.position = "";
        this.email = "";
    }
}