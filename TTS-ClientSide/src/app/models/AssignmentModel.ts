export class AssignmentModel{
    assignmentId : number;
    assignmentName : string;
    assignmentDescription : string;
    assignmentStatus : string;
    deadline : Date;
    employeeId : number;
    projectId : number;

    constructor(){
        this.assignmentId = 0;
        this.assignmentName = "";
        this.assignmentDescription = "";
        this.assignmentStatus = "";
        this.deadline = new Date();
        this.employeeId = 0;
        this.projectId = 0;
    }
}