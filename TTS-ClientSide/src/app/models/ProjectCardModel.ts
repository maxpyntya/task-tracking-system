export class ProjectCardModel{
    projectId : number;
    projectName : string;
    projectDescription : string;
    startDate : Date;
    closeDate? : Date;
    currentEmployeePosition : string;
    completionPercentage : number;

    constructor(){
        this.projectId =0;
        this.projectName = "";
        this.projectDescription = "";
        this.startDate = new Date();
        this.currentEmployeePosition = "";
        this.completionPercentage = 0;
    }
}