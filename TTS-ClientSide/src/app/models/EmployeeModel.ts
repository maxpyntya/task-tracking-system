export class EmployeeModel{
    id : number;
    firstName : string;
    lastName : string;
    userName : string;
    email : string;

    constructor(){
        this.id = 0;
        this.firstName = "";
        this.lastName = "";
        this.userName = "";
        this.email = "";
    }
}