import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeWorkingSpaceAssignmentsComponent } from './employee-working-space-assignments.component';

describe('EmployeeWorkingSpaceAssignmentsComponent', () => {
  let component: EmployeeWorkingSpaceAssignmentsComponent;
  let fixture: ComponentFixture<EmployeeWorkingSpaceAssignmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeWorkingSpaceAssignmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeWorkingSpaceAssignmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
