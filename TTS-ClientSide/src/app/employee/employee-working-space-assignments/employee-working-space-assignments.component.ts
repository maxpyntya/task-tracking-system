import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {ProjectWithTasksModel} from "src/app/models/ProjectWithTasksModel";
import { AssignmentClassDefinerService } from 'src/app/services/assignment-class-definer.service';
import { EmployeeService } from 'src/app/services/employee.service';
@Component({
  selector: 'app-working-space-assignments',
  templateUrl: './employee-working-space-assignments.component.html',
  styleUrls: ['./employee-working-space-assignments.component.css']
})
export class EmployeeWorkingSpaceAssignmentsComponent implements OnInit {

  projectWithTasks : ProjectWithTasksModel;

  constructor(private empService : EmployeeService, private route : ActivatedRoute) {
   }

  ngOnInit(): void {
    let projectId = +this.route.snapshot.paramMap.get("projectId")!;
    let pageNumString = this.route.snapshot.paramMap.get("taskPageNum");
    let taskPageNum = pageNumString === null || undefined ? 0 : +pageNumString;

    this.empService.getProject(projectId,taskPageNum).subscribe(resp=>this.projectWithTasks = resp);
  }

}
