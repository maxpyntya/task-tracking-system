import { Component, Input, OnInit } from '@angular/core';
import { EmployeeModel } from 'src/app/models/EmployeeModel';

@Component({
  selector: 'app-employee-card',
  templateUrl: './employee-card.component.html',
  styleUrls: ['./employee-card.component.css', '../../shared/styles/employee-card.css']
})
export class EmployeeCardComponent implements OnInit {

  @Input() employee : EmployeeModel;
  constructor() { }

  ngOnInit(): void {
  }

}
