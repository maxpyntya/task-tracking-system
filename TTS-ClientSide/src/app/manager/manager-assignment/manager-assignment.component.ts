import { Component, Input, OnInit } from '@angular/core';
import { AssignmentModel } from 'src/app/models/AssignmentModel';
import { AssignmentClassDefinerService } from 'src/app/services/assignment-class-definer.service';
import { ManagerService } from 'src/app/services/manager.service';

@Component({
  selector: 'app-manager-assignment',
  templateUrl: './manager-assignment.component.html',
  styleUrls: ['./manager-assignment.component.css', '../../shared/styles/assignment.css', '../../shared/styles/assignment-status.css']
})
export class ManagerAssignmentComponent implements OnInit {
  
  @Input() assignment : AssignmentModel;

  constructor(private managerService : ManagerService, public assignmentClassDefiner : AssignmentClassDefinerService) { }

  ngOnInit(): void {
  }

}
