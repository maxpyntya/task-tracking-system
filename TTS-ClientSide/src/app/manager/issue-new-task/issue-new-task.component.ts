import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AssignmentModel } from 'src/app/models/AssignmentModel';
import { EmployeeModel } from 'src/app/models/EmployeeModel';
import { SelectedEmployeeTransferInfo } from 'src/app/SelectedEmployeeTransferInfo';
import { GetClickedEmployeeIdService } from 'src/app/services/get-clicked-employee-id.service';
import { ManagerService } from 'src/app/services/manager.service';
import { checkIfDateIsCorrect} from 'src/app/validators/dateValidator'
import { showSuccessMessage, hideSuccessMessage } from 'src/app/successMessageFunctions';
import { showLoadingBar, hideLoadingBar } from 'src/app/loadingBarFunctions';

@Component({
  selector: 'app-issue-new-task',
  templateUrl: './issue-new-task.component.html',
  styleUrls: ['./issue-new-task.component.css', '../employee-card/employee-card.component.css']
})
export class IssueNewTaskComponent implements OnInit {

  employeeSelected : boolean = false;
  selectedEmployeeId : number;
  projId : number;
  employees : EmployeeModel[];

  previousClickedEmployee : HTMLElement;

  selectedEmployeeInfo : SelectedEmployeeTransferInfo = new SelectedEmployeeTransferInfo();

  assignmentForm : FormGroup = new FormGroup({
    assignmentName : new FormControl("",Validators.required),
    assignmentDescription : new FormControl("",Validators.required),
    assignmentDeadline : new FormControl("", checkIfDateIsCorrect()),
    employeeFirstName : new FormControl("",Validators.required),
    employeeLastName : new FormControl("",Validators.required)
  });
  constructor(private router : Router, private managerService : ManagerService, private getClickedEmployeeService : GetClickedEmployeeIdService) {
    this.projId = +(this.router.getCurrentNavigation()?.extras.state as any).projId;
   }

  ngOnInit(): void {
  }

  findEmployees(){
    if(this.assignmentForm.invalid){
      return;
    }
    let firstName = this.assignmentForm.controls['employeeFirstName'].value;
    let lastName = this.assignmentForm.controls['employeeLastName'].value;

    this.managerService.findEmployeesByNameOnProject(firstName, lastName, this.projId).subscribe(resp=>this.employees=resp);
  }

  getClickedEmployeeId(event : Event){
    this.getClickedEmployeeService.getClickedEmployeeId(event, this.selectedEmployeeInfo);
  }

  issueTask(){
    hideSuccessMessage();
    if(this.assignmentForm.invalid){
      this.showAllValidationErrors();
      return;
    }
    if(!this.employeeSelected){
      let errorMessage = document.getElementById('employee-not-selected-error') as HTMLElement;
      errorMessage.style.display = 'block';
      return;
    }
    showLoadingBar();
    let assignment : AssignmentModel = this.getAssignmentFromForm();
    this.selectedEmployeeId = this.selectedEmployeeInfo.selectedEmployeeId;
    
    this.managerService.issueTaskToEmployee(this.selectedEmployeeId,assignment).subscribe(resp=>{hideLoadingBar(); showSuccessMessage();},error=>{hideLoadingBar(); console.log(error.error);});
  }

  getAssignmentFromForm() : AssignmentModel{
    let assignment : AssignmentModel = new AssignmentModel();
    assignment.assignmentName = this.assignmentForm.controls['assignmentName'].value;
    assignment.assignmentDescription = this.assignmentForm.controls['assignmentDescription'].value;
    assignment.deadline = this.assignmentForm.controls['assignmentDeadline'].value;
    assignment.assignmentStatus = "New";
    assignment.projectId = this.projId;
    return assignment;
  }

  showAllValidationErrors(){
    this.assignmentForm.markAllAsTouched();
  }
}
