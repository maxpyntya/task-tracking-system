import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueNewTaskComponent } from './issue-new-task.component';

describe('IssueNewTaskComponent', () => {
  let component: IssueNewTaskComponent;
  let fixture: ComponentFixture<IssueNewTaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IssueNewTaskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueNewTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
