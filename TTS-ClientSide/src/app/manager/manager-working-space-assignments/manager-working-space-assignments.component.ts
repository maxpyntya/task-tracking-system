import { Component,  Input, OnInit, Type } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectWithTasksModel } from 'src/app/models/ProjectWithTasksModel';
import { ManagerService } from 'src/app/services/manager.service';
import { AssignEmployeeToProjectComponent } from '../assign-employee-to-project/assign-employee-to-project.component';

@Component({
  selector: 'app-manager-working-space-assignments',
  templateUrl: './manager-working-space-assignments.component.html',
  styleUrls: ['./manager-working-space-assignments.component.css', '../../shared/styles/working-space-assignments.css']
})
export class ManagerWorkingSpaceAssignmentsComponent implements OnInit {

  constructor(private managerService : ManagerService, private router : ActivatedRoute) { }

  @Input() projectWithTasks : ProjectWithTasksModel;
  @Input()isAssignEmpToProjVisible : boolean = false;

  ngOnInit(): void {
    let projId = +this.router.snapshot.paramMap.get("projectId")!;
    let stringPageNum = this.router.snapshot.paramMap.get("taskPageNum");
    let taskPageNum = stringPageNum === null ? 0 : +stringPageNum;
    this.managerService.getProject(projId,taskPageNum).subscribe(resp=>this.projectWithTasks=resp);
  }

  showAssignEmployeeToProjectModal(){
    this.isAssignEmpToProjVisible = true;
  }

  removeAssignEmployeeToProjectModal(){
    this.isAssignEmpToProjVisible = false;
  }

}
