import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerWorkingSpaceAssignmentsComponent } from './manager-working-space-assignments.component';

describe('ManagerWorkingSpaceAssignmentsComponent', () => {
  let component: ManagerWorkingSpaceAssignmentsComponent;
  let fixture: ComponentFixture<ManagerWorkingSpaceAssignmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagerWorkingSpaceAssignmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerWorkingSpaceAssignmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
