import { Component, OnInit, Output, EventEmitter, Input, Attribute} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EmployeeModel } from 'src/app/models/EmployeeModel';
import { PositionModel } from 'src/app/models/PositionModel';
import { SelectedEmployeeTransferInfo } from 'src/app/SelectedEmployeeTransferInfo';
import { GetClickedEmployeeIdService } from 'src/app/services/get-clicked-employee-id.service';
import { ManagerService } from 'src/app/services/manager.service';
import { EmployeeCardComponent } from '../employee-card/employee-card.component';
import { showSuccessMessage, hideSuccessMessage } from 'src/app/successMessageFunctions';
import { showLoadingBar, hideLoadingBar } from 'src/app/loadingBarFunctions';

@Component({
  selector: 'app-assign-employee-to-project',
  templateUrl: './assign-employee-to-project.component.html',
  styleUrls: ['./assign-employee-to-project.component.css', '../../shared/styles/find-employees.css', '../employee-card/employee-card.component.css']
})
export class AssignEmployeeToProjectComponent implements OnInit {

  findEmployeesForm : FormGroup = new FormGroup({
    firstName : new FormControl("",Validators.required),
    lastName : new FormControl("",Validators.required)
  });

  findPositionForm : FormGroup = new FormGroup({
    positionName : new FormControl("",Validators.required)
  });

  @Output() closeEvent = new EventEmitter();
  @Input() employees : EmployeeModel[];
  positions : PositionModel[];
  @Input() projectId : number;
  selectedEmployeeId : number;
  selectedPositionId : number;

  previousClickedEmployee : HTMLElement;
  employeeSelected : boolean;


  selectedEmployeeInfo : SelectedEmployeeTransferInfo = new SelectedEmployeeTransferInfo();

  constructor(private managerService : ManagerService, private getClickedEmployeeService : GetClickedEmployeeIdService) { }

  ngOnInit(): void {
    
  }

  assignEmployee(){
    this.selectedEmployeeId = this.selectedEmployeeInfo.selectedEmployeeId;
    hideSuccessMessage();

    if(!this.employeeSelected){
      let errorMessage = document.getElementById('employee-not-selected-error') as HTMLElement;
      errorMessage.style.display = 'block';
      return;
    }

    if(this.findPositionForm.invalid || this.findEmployeesForm.invalid){
      this.showAllValidationErrors();
      return;
    }

    if(this.selectedPositionId === null || this.selectedPositionId === undefined || Number.isNaN(this.selectedPositionId)){
      let errorMessage = document.getElementById('position-not-selected-error') as HTMLElement;
      errorMessage.style.display = 'block';
      return;
    }

    showLoadingBar();

    if(this.projectId != null || undefined && this.selectedEmployeeId != null || undefined && this.selectedPositionId != null || undefined){
      this.managerService.assignEmployeeToProject(this.projectId,this.selectedEmployeeId,this.selectedPositionId).subscribe(res=>{hideLoadingBar(); showSuccessMessage();});
    }
  }

  findEmployees(){
    if(this.findEmployeesForm.invalid){
      this.findEmployeesForm.markAllAsTouched();
      return;
    }
    
    let firstName = this.findEmployeesForm.controls["firstName"].value;
    let lastName = this.findEmployeesForm.controls["lastName"].value;

    this.managerService.findEmployeesByNameToAssignProject(firstName,lastName,this.projectId).subscribe(resp=>this.employees=resp);
  }

  findPositions(){
    if(this.findPositionForm.invalid){
      this.findPositionForm.markAllAsTouched();
      return;
    }
    let position = this.findPositionForm.controls['positionName'].value;
    let container = document.getElementById('found-positions') as HTMLElement;
    container.style.display = 'block';
    this.managerService.findPositionsByName(position).subscribe(resp=>this.positions=resp);
  }

  getEmployeeId(event: Event){
    this.getClickedEmployeeService.getClickedEmployeeId(event, this.selectedEmployeeInfo);

    this.selectedEmployeeId = this.selectedEmployeeInfo.selectedEmployeeId;

    console.log('position : '+this.selectedPositionId);
    console.log(this.selectedEmployeeId);
  }

  getPositionId(event : Event){
    let option = event.currentTarget as any;
    this.selectedPositionId = +option.value;
  }

  showAllValidationErrors(){
    this.findPositionForm.markAllAsTouched();
    this.findEmployeesForm.markAllAsTouched();
  }

  close(event : Event){
    if(event.target == document.getElementsByClassName('modal-outer')[0])
      this.closeEvent.emit();
  }

}
