import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import decode from 'jwt-decode';
import { SpecifyRoleService } from 'src/app/services/specify-role.service';

@Injectable({
  providedIn: 'root'
})
export class RoleRedirectGuard implements CanActivate {

  constructor(private roleSpecifier : SpecifyRoleService) {
      
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree 
  {
    var isInRole = this.roleSpecifier.hasRole(route.data.expectedRole);
    if(isInRole)
      return true;

    return false;
    
  }
  
}
