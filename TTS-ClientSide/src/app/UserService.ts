import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ProjectCardModel } from "./models/ProjectCardModel";
import { ProjectWithTasksModel } from "./models/ProjectWithTasksModel";

@Injectable()
export abstract class UserService{
    abstract getProjects() : Observable<ProjectCardModel[]>;
    abstract getProject(projectId : number,tasksPageNum : number) : Observable<ProjectWithTasksModel>;
}