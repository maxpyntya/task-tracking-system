export function showSuccessMessage(){
    let container = document.getElementsByClassName('operation-successful-wrapper')[0] as HTMLElement;
    container.style.display='block';
}

export function hideSuccessMessage(){
    let container = document.getElementsByClassName('operation-successful-wrapper')[0] as HTMLElement;
    container.style.display='none';
}