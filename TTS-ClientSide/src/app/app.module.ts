import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'; 
import { TTSInterceptor } from './TTSInterceptor';
import { EmployeeComponent } from './employee/employee.component';
import { WorkingSpaceProjectsComponent } from './components/working-space-projects/working-space-projects.component';
import { EmployeeWorkingSpaceAssignmentsComponent } from './employee/employee-working-space-assignments/employee-working-space-assignments.component';
import { MainComponent } from './components/main/main.component';
import { ProjectCardComponent } from './components/project-card/project-card.component';
import { AssignmentComponent } from './components/assignment/assignment.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { ManagerComponent } from './manager/manager.component';
import { ManagerWorkingSpaceAssignmentsComponent } from './manager/manager-working-space-assignments/manager-working-space-assignments.component';
import { ManagerAssignmentComponent } from './manager/manager-assignment/manager-assignment.component';
import { AssignEmployeeToProjectComponent } from './manager/assign-employee-to-project/assign-employee-to-project.component';
import { EmployeeCardComponent } from './manager/employee-card/employee-card.component';
import { IssueNewTaskComponent } from './manager/issue-new-task/issue-new-task.component';
import { AdminComponent } from './admin/admin.component';
import { AdminWorkingSpaceAssignmentsComponent } from './admin/admin-working-space-assignments/admin-working-space-assignments.component';
import { ChangeRolesComponent } from './admin/change-roles/change-roles.component';
import { EmployeeWithPositionCardComponent } from './admin/employee-with-position-card/employee-with-position-card.component';
import { AddNewProjectComponent } from './admin/add-new-project/add-new-project.component';
import { ConfirmProjectDeletionModalComponent } from './admin/confirm-project-deletion-modal/confirm-project-deletion-modal.component';
import { SwitchToAnotherRoleComponent } from './components/switch-to-another-role/switch-to-another-role.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { OperationSuccessfulComponent } from './components/operation-successful/operation-successful.component';
import { LoadingBarComponent } from './components/loading-bar/loading-bar.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    EmployeeComponent,
    MainComponent,
    WorkingSpaceProjectsComponent,
    EmployeeWorkingSpaceAssignmentsComponent,
    ProjectCardComponent,
    AssignmentComponent,
    ProgressBarComponent,
    ManagerComponent,
    ManagerWorkingSpaceAssignmentsComponent,
    ManagerAssignmentComponent,
    AssignEmployeeToProjectComponent,
    EmployeeCardComponent,
    IssueNewTaskComponent,
    AdminComponent,
    AdminWorkingSpaceAssignmentsComponent,
    ChangeRolesComponent,
    EmployeeWithPositionCardComponent,
    AddNewProjectComponent,
    ConfirmProjectDeletionModalComponent,
    SwitchToAnotherRoleComponent,
    PageNotFoundComponent,
    OperationSuccessfulComponent,
    LoadingBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [{provide : HTTP_INTERCEPTORS, useClass : TTSInterceptor, multi : true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
