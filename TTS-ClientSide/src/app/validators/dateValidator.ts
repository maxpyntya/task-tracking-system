import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export function checkIfDateIsCorrect(): ValidatorFn {
    return (control:AbstractControl) : ValidationErrors | null => {
        let dateToCheck = new Date(control.value).setHours(0,0,0,0);
        let currentDate = new Date().setHours(0,0,0,0);
        return dateToCheck <= currentDate ? {incorrectDate : "Deadline cannot be set as past date or today's date"} : null;
    }}

export function checkDateForProjectStart(): ValidatorFn {
    return (control:AbstractControl) : ValidationErrors | null => {
        let dateToCheck = new Date(control.value).setHours(0,0,0,0);
        let currentDate = new Date().setHours(0,0,0,0);
        return dateToCheck > currentDate ? {incorrectDate : "Project cannot be started in future time"} : null;
    }}
    
