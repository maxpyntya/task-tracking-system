import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EmployeeRegisterModel } from '../EmployeeRegisterModel';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css', '../../loading-spinner-animation.css', '../../shared/styles/auth-styles.css']
})
export class RegisterComponent implements OnInit {

  employee : EmployeeRegisterModel;

  registerForm : FormGroup = new FormGroup({
    "FirstName" : new FormControl("",[Validators.required]),
    "LastName" : new FormControl("",[Validators.required]),
    "Username" : new FormControl("",[Validators.required]),
    "Email" : new FormControl("",[Validators.required, Validators.email]),
    "Password" : new FormControl("",[Validators.required])
  });

  constructor(private authService : AuthService) {
    this.employee = new EmployeeRegisterModel();
   }


  ngOnInit(): void {
  }

  submit(){
    if(!this.registerForm.invalid){
      this.createEmployee();
      this.startSpinnerAnimation();
      this.toggleDisableButton();
      this.authService.register(this.employee).subscribe(response=>{console.log(response.body); this.toggleDisableButton();},errors=>{
        this.stopSpinnerAnimation();
        this.toggleDisableButton();
        var errorContainer = document.getElementsByClassName('response-error')[0] as HTMLInputElement;
        errorContainer.style.display = 'block';
        errorContainer.innerHTML = errors.error;
      });
    }
  }
  
  startSpinnerAnimation(){
    var errorContainer = document.getElementsByClassName('loading-spinner')[0] as HTMLInputElement;
    errorContainer.style.display = "block";
  }

   stopSpinnerAnimation() {
    var errorContainer = document.getElementsByClassName('loading-spinner')[0] as HTMLInputElement;
    errorContainer.style.display = "none";

}
  createEmployee(){
    this.employee.FirstName = this.registerForm.controls["FirstName"].value;
    this.employee.LastName = this.registerForm.controls["LastName"].value;
    this.employee.Username = this.registerForm.controls["Username"].value;
    this.employee.Email = this.registerForm.controls["Email"].value;
    this.employee.Password = this.registerForm.controls["Password"].value;
  }

  toggleDisableButton(){
    let element = document.getElementById('submit') as any;
    element.disabled = !element.disabled;
  }

}


