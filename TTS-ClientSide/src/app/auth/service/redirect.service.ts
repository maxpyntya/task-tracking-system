import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RedirectService {

  constructor(private router : Router) { }

  redirectAccordingToRole(roles : string){
    let role = roles.split(',')[0].toLowerCase();
    this.router.navigate(["/"+role]);
  }
}
