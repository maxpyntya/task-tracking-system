import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmployeeLoginModel } from '../EmployeeLoginModel';
import { EmployeeRegisterModel } from '../EmployeeRegisterModel';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private registerUrl = "https://localhost:44393/api/Account/Register";
  private loginUrl = "https://localhost:44393/api/Account/Login";

  readonly headers : HttpHeaders = new HttpHeaders({
    "Content-Type" : "application/json",
  });

  constructor(private httpClient : HttpClient) { }

  register(emp : EmployeeRegisterModel) : Observable<HttpResponse<Object>>{
    this.startLoadingAnimation();
    return this.httpClient.post(this.registerUrl,emp,{headers : this.headers, observe : 'response'});
  }

  login(emp : EmployeeLoginModel) : Observable<HttpResponse<Object>>{
    this.startLoadingAnimation;
    return this.httpClient.post(this.loginUrl,emp,{headers : this.headers, observe : 'response'});
  }
  
  startLoadingAnimation(){
    var errorContainer = document.getElementsByClassName('loading-spinner')[0] as HTMLInputElement;
    errorContainer.style.display = "block";
  }
}
