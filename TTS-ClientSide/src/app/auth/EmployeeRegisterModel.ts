export class EmployeeRegisterModel{

    constructor (){
        this.FirstName = "";
        this.LastName = "";
        this.Username = "";
        this.Email = "";
        this.Password = "";
    }

    FirstName : string;
    LastName : string;
    Username : string;
    Email : string;
    Password : string;
}