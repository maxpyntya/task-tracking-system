import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EmployeeLoginModel } from '../EmployeeLoginModel';
import { AuthService } from '../service/auth.service';
import { RedirectService } from '../service/redirect.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css','../../loading-spinner-animation.css', '../../shared/styles/auth-styles.css']
})
export class LoginComponent implements OnInit {
  show : boolean = false;
  employee : EmployeeLoginModel = new EmployeeLoginModel();

  loginForm : FormGroup = new FormGroup({
    Username : new FormControl("",[Validators.required]),
    Password : new FormControl("",[Validators.required])
  });

  constructor(private authService : AuthService, private redirector : RedirectService) { }

  ngOnInit(): void {
  }

  submit(){

    if(!this.loginForm.invalid){
      this.fillEmployeeModel(); 
      this.startSpinnerAnimation();
      this.toggleBlockButton();

      this.authService.login(this.employee).subscribe(response=>
        {
          this.stopSpinnerAnimation();
          this.toggleBlockButton();

          let userInfo : any = JSON.parse(JSON.stringify(response.body));
          localStorage.setItem("Token",userInfo["Token"]);
          this.redirector.redirectAccordingToRole(userInfo["Roles"]);
        },
        error=>
        {
        this.stopSpinnerAnimation();
        this.toggleBlockButton();

        var container = document.getElementsByClassName("response-error")[0] as HTMLInputElement;
        container.style.display = 'block';
        container.innerHTML = error.error;
      });
    }
    

  }

  //may use spinner as separate component
  stopSpinnerAnimation() {
    var errorContainer = document.getElementsByClassName('loading-spinner')[0] as HTMLInputElement;
    errorContainer.style.display = "none";
}

  startSpinnerAnimation(){
    var errorContainer = document.getElementsByClassName('loading-spinner')[0] as HTMLInputElement;
    errorContainer.style.display = "block";
  }

  fillEmployeeModel(){
    this.employee.Username = this.loginForm.controls['Username'].value;
    this.employee.Password = this.loginForm.controls['Password'].value;
  }

  toggleShowPassword(){
    this.show = !this.show;
  }

  toggleBlockButton(){
    let button = document.getElementById('submit')as any;
    button.disabled = !button.disabled;
  }
}
