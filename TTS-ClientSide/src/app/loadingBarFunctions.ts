export function showLoadingBar(){
    let container = document.getElementsByClassName('loading-bar-wrapper')[0] as HTMLElement;
    container.style.display = 'block';
}

export function hideLoadingBar(){
    let container = document.getElementsByClassName('loading-bar-wrapper')[0] as HTMLElement;
    container.style.display = 'none';
}