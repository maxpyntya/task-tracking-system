﻿using BLL.Interfaces;
using BLL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Filters;
using WebAPI.Interfaces;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer", Roles = "Manager")]
    [GetUserIdFilter]
    public class ManagerController : ControllerBase, IUserController<IManagerService>
    {
        public IManagerService Service { get; private set; }
        readonly int numberOfProjectsPerPage;
        readonly int numberOfAssignmentsPerPage;
        public ManagerController(IManagerService service, IConfiguration config)
        {
            Service = service;
            numberOfProjectsPerPage = Int32.Parse(config.GetSection("Pagination")["Projects"]);
            numberOfAssignmentsPerPage = Int32.Parse(config.GetSection("Pagination")["Assignments"]);
        }

        [HttpGet("Projects")]
        public IActionResult GetProjects()
        {
            try
            {
                var projects = Service.ShowProjects(numberOfProjectsPerPage);
                return new JsonResult(projects);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpGet("Project/{projectId}/{pageNum?}")]
        public async Task<IActionResult> GetProject(int projectId, int pageNum = 0)
        {
            try
            {
                var project = await Service.GetProjectAsync(projectId, numberOfAssignmentsPerPage, pageNum);
                return new JsonResult(project);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpPost("Task/{employeeId}")]
        public async Task<IActionResult> IssueNewTaskToEmployee(int employeeId, [FromBody] AssignmentModel assignment)
        {
            try
            {
                await Service.IssueNewTaskToEmployeeAsync(employeeId, assignment);
                return StatusCode(201);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpPost("AssignProject/{projectId}/{employeeId}/{positionId}")]
        public async Task<IActionResult> AssignEmployeeToProject(int projectId, int employeeId, int positionId)
        {
            try
            {
                await Service.AssignEmployeeToProjectAsync(projectId, employeeId, positionId);
                return StatusCode(201);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }
        [HttpGet("Employees/{projectId}")]
        public IActionResult GetEmployeesByName(int projectId, string firstName, string lastName)
        {
            try
            {
                var employees = Service.FindEmployeesByNameToAssignProject(projectId, firstName, lastName);
                return new JsonResult(employees);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpGet("PresentEmployees/{projectId}")]
        public IActionResult FindEmployeesByNameOnProject(int projectId, string firstName, string lastName)
        {
            try
            {
                var employees = Service.FindEmployeesByNameOnProject(projectId, firstName, lastName);
                return new JsonResult(employees);
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpPost("Task/{assignmentId}/{employeeId}")]
        public async Task<IActionResult> IssueExistingTaskToEmployeeAsync(int employeeId, int assignmentId)
        {
            try
            {
                await Service.IssueExistingTaskToEmployeeAsync(employeeId, assignmentId);
                return StatusCode(201);
            }
            catch(Exception ex)
            {
                return Content(ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }
        [HttpGet("Positions")]
        public IActionResult FindPositionsByName(string position)
        {
            try
            {
                var positionsFound = Service.FindPositionsByName(position);
                return new JsonResult(positionsFound);
            }
            catch (Exception ex)
            {

                return Content(ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

    }
}
