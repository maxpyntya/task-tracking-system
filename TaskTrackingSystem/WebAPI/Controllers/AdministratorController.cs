﻿using BLL.Interfaces;
using BLL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Filters;
using WebAPI.Interfaces;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer", Roles = "Admin")]
    [GetUserIdFilter]
    public class AdministratorController : ControllerBase, IUserController<IAdministratorService>
    {
        public IAdministratorService Service { get; private set; }
        readonly int numberOfProjectsPerPage;
        readonly int numberOfAssignmentsPerPage;

        public AdministratorController(IAdministratorService service, IConfiguration config)
        {
            Service = service;
            numberOfAssignmentsPerPage = Int32.Parse(config.GetSection("Pagination").GetValue<string>("Assignments"));
            numberOfProjectsPerPage = Int32.Parse(config.GetSection("Pagination").GetValue<string>("Projects"));
        }

        [HttpPost("Project/New")]
        public async Task<IActionResult> AddNewProject([FromBody] ProjectCreationModel project)
        {
            try
            {
                await Service.AddProjectAsync(project);
                return StatusCode(201);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpDelete("Project/Delete/{projectId}")]
        public async Task<IActionResult> DeleteProject(int projectId)
        {
            try
            {
                await Service.DeleteProjectAsync(projectId);
                return StatusCode(200);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpGet("Projects")]
        public IActionResult ShowProjects()
        {
            try
            {
                var projects = Service.ShowProjects(numberOfProjectsPerPage);
                return new JsonResult(projects);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpGet("Project/{projectId}/{taskPageNum?}")]
        public async Task<IActionResult> GetProjectAsync(int projectId, int taskPageNum)
        {
            try
            {
                var desiredProject = await Service.GetProjectAsync(projectId, numberOfAssignmentsPerPage, taskPageNum);
                return new JsonResult(desiredProject);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpPost("Project/RemoveFromRole/{projectId}/{employeeId}")]
        public async Task<IActionResult> RemoveEmployeeFromRoleAsync(int projectId, int employeeId)
        {
            try
            {
                await Service.RemoveEmployeeFromRoleAsync(employeeId, projectId);
                return StatusCode(204);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpPost("Project/SetRole/{projectId}/{employeeId}/{roleId}/{positionId?}")]
        public async Task<IActionResult> SetEmployeeRole(int projectId, int employeeId, int roleId, int? positionId)
        {
            try
            {
                await Service.SetEmployeeRoleAsync(employeeId, projectId, roleId, positionId);
                return StatusCode(201);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpGet("Project/{projectId}/Positions")]
        public IActionResult GetAvailablePositions(int projectId, string positionToFind)
        {
            try
            {
                var positions = Service.ShowAvailablePositionsByName(projectId, positionToFind);
                return new JsonResult(positions);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpGet("Project/{projectId}/Roles")]
        public IActionResult GetAvailableRoles(int projectId)
        {
            try
            {
                var roles = Service.ShowAvailableRoles(projectId);
                return new JsonResult(roles);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpGet("Employees/{projectId}")]
        public IActionResult FindEmployeesByNameOnProject(int projectId, string firstName, string lastName)
        {
            try
            {
                var employees = Service.FindEmployeesWithPositionsAndRolesByNameOnProject(projectId, firstName, lastName);
                return new JsonResult(employees);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }
    }
}
