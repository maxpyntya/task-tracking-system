﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NamedConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Interfaces;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AccountController : ControllerBase
    {
        IAuthenticationService _authenticationService;
        IJwtIssuer _jwtIssuer;
        public AccountController(IAuthenticationService authService, IJwtIssuer jwtIssuer)
        {
            _authenticationService = authService;
            _jwtIssuer = jwtIssuer;
        }

        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] EmployeeRegisterModel empModel)
        {
            using (_authenticationService)
            {
                var result = await _authenticationService.AddUserAsync(empModel);
                if (result.Result == OperationResultEnum.Failed)
                    return StatusCode(400,result.ErrorMessage);
                return Content("Registration successfull!");
            }
        }

        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] EmployeeLoginModel model)
        {
            using (_authenticationService)
            {
                var result = await _authenticationService.LoginUserAsync(model);
                if (result.Result == OperationResultEnum.Failed)
                    return StatusCode(400,result.ErrorMessage);
                var jwtAndRoles = _jwtIssuer.GetTokenAndRoles(result.Payload);
                
                return new JsonResult(jwtAndRoles);
            }
        }
    }
}
