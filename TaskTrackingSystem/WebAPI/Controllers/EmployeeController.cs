﻿using BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebAPI.Filters;
using WebAPI.Interfaces;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes ="Bearer", Roles ="Employee")]
    [GetUserIdFilter]
    public class EmployeeController : ControllerBase, IUserController<IEmployeeService>
    {
        public IEmployeeService Service { get; private set; }
        readonly int numberOfProjectsPerPage;
        readonly int numberOfAssignmentsPerPage;

        public EmployeeController(IEmployeeService service, IConfiguration config)
        {
            Service = service;
            numberOfProjectsPerPage = Int32.Parse(config.GetSection("Pagination")["Projects"]);
            numberOfAssignmentsPerPage = Int32.Parse(config.GetSection("Pagination")["Assignments"]);
        }

        [HttpGet("Project/{projectId}/{taskPageNum?}")]
        public async Task<IActionResult> GetProject(int projectId, int taskPageNum=0)
        {
            try
            {
                var project = await Service.GetProjectAsync(projectId,numberOfAssignmentsPerPage,taskPageNum);
                return new JsonResult(project);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }

        [HttpGet("Projects")]
        public IActionResult ShowProjects()
        {
            try
            {  
                var projects = Service.ShowProjects(numberOfProjectsPerPage);
                return new JsonResult(projects);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }
        [HttpPut("Task/{taskId}")]
        public async Task<IActionResult> SetTaskStatusAsync(int taskId, string newStatus)
        {
            try
            {
                await Service.SetTaskStatusAsync(taskId, newStatus);
                return StatusCode(200);
            }
            catch(Exception ex)
            {
                return Content(ex.Message);
            }
            finally
            {
                Service.Dispose();
            }
        }
    }
}
