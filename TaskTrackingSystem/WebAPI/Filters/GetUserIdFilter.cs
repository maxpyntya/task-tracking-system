﻿using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebAPI.Interfaces;

namespace WebAPI.Filters
{
    public class GetUserIdFilter : Attribute,IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var controller = ((IUserController<IUserService>)context.Controller);            
            controller.Service.UserId = GetUserId(context.HttpContext.User);
        } 
        private int GetUserId(ClaimsPrincipal user)
        {
            var id = user.Claims.Select(c => c.Subject.Claims).LastOrDefault().FirstOrDefault().Value;
            return Int32.Parse(id);
        }

    }
}
