﻿using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Interfaces
{
    public interface IUserController<out TService> where TService : IUserService
    {
        TService Service { get; }
    }
}
