﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Interfaces;

namespace WebAPI.Services
{
    public class JwtIssuer : IJwtIssuer
    {
        IConfiguration _configuration;
        public JwtIssuer(IConfiguration config)
        {
            _configuration = config;
        }
        public Dictionary<string, string> GetTokenAndRoles(Dictionary<string, string> requiredInfo)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration.GetValue<string>("Secret"));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new System.Security.Claims.ClaimsIdentity(GetClaims(requiredInfo)),
                Expires = DateTime.Now.AddMinutes(60),
                Issuer = _configuration.GetValue<string>("Issuer"),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var jwt = tokenHandler.WriteToken(token);

            Dictionary<string, string> jwtAndRoles = new Dictionary<string, string>
            {
                {"Token",jwt },
                {"Roles",requiredInfo["Roles"] }
            };

            return jwtAndRoles;
        }

        private Claim[] GetClaims(Dictionary<string,string> requiredInfo)
        {
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim (JwtRegisteredClaimNames.Sub,requiredInfo["Id"]));
            var roles = requiredInfo["Roles"].Split(',');
            foreach(string role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }
            return claims.ToArray();
        }

    }
}
