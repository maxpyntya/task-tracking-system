using DAL.Context_Classes;
using DAL.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL;
using BLL.Interfaces;
using BLL.Services;
using Microsoft.AspNetCore.Identity;
using DAL.Interfaces;
using DAL.AccountManagement;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using WebAPI.Services;
using WebAPI.Interfaces;
using DAL;

namespace WebAPI
{
    public class Startup
    {
        string path;
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            path = env.ContentRootPath;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddCors();

            string connectionString = GetConnectionString();

            services.AddDbContext<TTSContext>(opts =>
            {
                opts.UseSqlServer(connectionString);
            });

            services.AddIdentity<Employee, Role>().AddEntityFrameworkStores<TTSContext>();

            var mapperConfig = new MapperConfiguration(cfg => cfg.AddProfile(new AutomapperProfile()));
            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            var key = Encoding.ASCII.GetBytes(Configuration.GetValue<string>("Secret"));
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(opts =>
            {
                opts.TokenValidationParameters.ValidateLifetime = true;
                opts.TokenValidationParameters.RequireSignedTokens = true;
                opts.TokenValidationParameters.ValidateAudience = false;
                opts.TokenValidationParameters.ValidateIssuer = true;
                opts.TokenValidationParameters.ValidIssuer = Configuration.GetValue<string>("Issuer");
                opts.TokenValidationParameters.ValidateIssuerSigningKey = true;
                opts.TokenValidationParameters.IssuerSigningKey = new SymmetricSecurityKey(key);
            });

            services.AddOptions();

            services.AddTransient<IAuthentication<Employee>, AccountManager>();
            services.AddTransient<IAuthenticationService, AuthenticationService>();
            services.AddTransient<UserManager<Employee>>();
            services.AddTransient<SignInManager<Employee>>();
            services.AddTransient<IJwtIssuer, JwtIssuer>();
            services.AddTransient<IEmployeeService, EmployeeService>();
            services.AddTransient<IManagerService, ManagerService>();
            services.AddTransient<IAdministratorService, AdministratorService>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IEmailSender, EmailSender>();
        }

        private string GetConnectionString()
        {
            string connectionString = Configuration.GetConnectionString("TTS");
            int index = path.LastIndexOf("\\")+1;
            path = path.Remove(index);
            path += "DAL\\DB";
            connectionString = connectionString.Replace("%CONTENTROOTPATH%", path);
            return connectionString;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(cors => { cors.AllowAnyOrigin();
                cors.AllowAnyMethod();
                cors.AllowAnyHeader();
                
            });

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
