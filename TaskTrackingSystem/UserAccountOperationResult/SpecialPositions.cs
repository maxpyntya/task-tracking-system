﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NamedConstants
{
    public static class SpecialPositions
    {
        public static string Manager { get => "Manager"; }

        public static string[] GetSpecialPositions()
        {
            return new string[]
            {
                Manager
            };
        }
    }
}
