﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NamedConstants
{
    public class UserAccountOperationResult
    {
        public UserAccountOperationResult(OperationResultEnum resultType)
        {
            Result = resultType;
        }
        public UserAccountOperationResult(OperationResultEnum resultType, string errorMessage)
        {
            Result = resultType;
            ErrorMessage = errorMessage;
        }
        public UserAccountOperationResult(OperationResultEnum resultType, Dictionary<string,string> info)
        {
            Result = resultType;
            Payload = info;
        }


        public OperationResultEnum Result { get; set; }
        public Dictionary<string, string> Payload {get;set;}
        public string ErrorMessage { get; set; }
    }

    public enum OperationResultEnum
    {
        OK,
        Failed
    }

}

