﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NamedConstants
{
    public static class AssignmentStatus
    {
        public static string IsNew { get => "New"; }
        public static string IsDone { get => "Done"; }
        public static string NoEmployee { get => "No Employee"; }
    }
}
