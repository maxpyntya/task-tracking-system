﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL;
using BLL.Models;
using BLL.Services;
using DAL;
using DAL.Context_Classes;
using Microsoft.EntityFrameworkCore;
using NUnit;
using NUnit.Framework;
using Moq;
using BLL.Interfaces;

namespace TaskTrackingSystem.Tests
{
    [TestFixture]
    public partial class ManagerServiceTests
    {
        TTSContext context;
        DbContextOptions<TTSContext> dbOptions;
        UnitOfWork unitOfWork;
        IMapper mapper;
        Mock<IEmailSender> mockedEmailSender;

        [OneTimeSetUp]
        public void SetUp()
        {
            var mapperConfig = new MapperConfiguration(cfg => cfg.AddProfile(new AutomapperProfile()));
            mapper = mapperConfig.CreateMapper();
            mockedEmailSender = new Mock<IEmailSender>();
        }

        [SetUp]
        public void SetUpDbOptions()
        {
            dbOptions = new DbContextOptionsBuilder<TTSContext>()
            .UseInMemoryDatabase(Guid.NewGuid().ToString())
            .Options;

            context = new TTSContext(dbOptions);
            unitOfWork = new UnitOfWork(context);
            TestDbSeeder.SeedDb(context);
            context.SaveChanges();
        }

        [TearDown]
        public void TearDownDb()
        {
            context.Dispose();
        }

        [TestCase(1,2,40)]
        public async Task IssueTaskToEmployee_ShouldAddTaskToEmployeeAndUpdateProjectCompletionPercentage(int employeeId, int projectId, double expectedCompletionPercentage)
        {
            //Arrange
            ManagerService service = new ManagerService(unitOfWork, mapper, mockedEmailSender.Object);
            service.UserId = employeeId;

            AssignmentModel assignmentModel = new AssignmentModel
            {
                AssignmentName = "TestName",
                AssignmentDescription = "TestDescription",
                AssignmentStatus = "New",
                Deadline = DateTime.Now.AddDays(10),
                ProjectId = projectId
            };

            //Act
            await service.IssueNewTaskToEmployeeAsync(employeeId, assignmentModel);
            var actualAssignment= unitOfWork.AssignmentRepository.GetAll().FirstOrDefault(a => a.EmployeeId == employeeId && a.ProjectId == projectId && a.Deadline == assignmentModel.Deadline);
            var actualProject = await unitOfWork.ProjectRepository.GetByIdAsync(projectId);
            //Assert
            Assert.AreEqual(expectedCompletionPercentage, actualProject.PercentageOfCompletion);
            Assert.IsNotNull(actualAssignment);
        }

        [TestCase(3,2,1)]
        public async Task AssignEmployeeToProject_AddsNewRecordInEmployeeProjectPositionRoleTable(int employeeId, int projectId, int positionId)
        {
            //Arrange

            ManagerService service = new ManagerService(unitOfWork, mapper, mockedEmailSender.Object);
            service.UserId = employeeId;

            //Act
            await service.AssignEmployeeToProjectAsync(projectId, employeeId, positionId);
            var actualRecord = unitOfWork.JoinRepository.GetAll().FirstOrDefault(j => j.EmployeeId == employeeId && j.ProjectId == projectId);

            //Assert
            Assert.IsNotNull(actualRecord);
        }

        [TestCase(3,12)]
        public async Task IssueExistingTask_ShouldUpdateAssignment(int employeeId, int assignmentId)
        {
            //Arrange
            ManagerService service = new ManagerService(unitOfWork, mapper, mockedEmailSender.Object);

            //Act
            await service.IssueExistingTaskToEmployeeAsync(employeeId, assignmentId);
            var actualAssignment = await unitOfWork.AssignmentRepository.GetByIdAsync(assignmentId);

            //Assert
            Assert.AreEqual(employeeId, actualAssignment.EmployeeId);
        }

        [TestCase(2,3,4, "Developer")]
        public async Task ChangeEmployeePosition_ShouldChangePosition(int employeeId, int projectId, int positionId, string expectedPositionName)
        {
            //Arrange
            ManagerService service = new ManagerService(unitOfWork, mapper, mockedEmailSender.Object);

            //Act
            await service.ChangeEmployeePositionAsync(employeeId, projectId, positionId);
            var joinTableRecord = await unitOfWork.JoinRepository.GetByIdAsync(employeeId, projectId);
            var actualPosition = joinTableRecord.Position.PositionName;

            //Assert
            Assert.AreEqual(expectedPositionName, actualPosition);
        }


    }
}
