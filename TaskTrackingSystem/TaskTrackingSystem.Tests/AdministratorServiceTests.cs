﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL;
using BLL.Models;
using BLL.Services;
using DAL;
using DAL.Context_Classes;
using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace TaskTrackingSystem.Tests
{
    [TestFixture]
    public class AdministratorServiceTests
    {
        TTSContext _context;
        IMapper _mapper;
        IUnitOfWork _unitOfWork;

        [OneTimeSetUp]
        public void SetUpMapper()
        {
            var mapperConfig = new MapperConfiguration(cfg => cfg.AddProfile(new AutomapperProfile()));
            _mapper = mapperConfig.CreateMapper();
        }

        [SetUp]
        public void SetUpDb()
        {
            DbContextOptions<TTSContext> options = new DbContextOptionsBuilder<TTSContext>().UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            _context = new TTSContext(options);
            TestDbSeeder.SeedDb(_context);
            _context.SaveChanges();
            _unitOfWork = new UnitOfWork(_context);
        }

        [TearDown]
        public void DisposeDb()
        {
            _context.Dispose();
        }

        [TestCase(1,2,null)]
        public async Task RemoveEmployeFromRole_ShouldDeleteEntryInJoinTableAndUpdateAssignmentsTable(int employeeId, int projectId, object expectedJoinTableRecord)
        {
            //Arrange
            AdministratorService service = new AdministratorService(_unitOfWork, _mapper);

            //Act
            await service.RemoveEmployeeFromRoleAsync(employeeId, projectId);
            var actualJoinTableRecord = await _unitOfWork.JoinRepository.GetByIdAsync(employeeId, projectId);
            var actualAssignmentRecords = _unitOfWork.AssignmentRepository.GetAll().Where(a => a.EmployeeId == null && a.ProjectId == projectId);

            //Assert
            Assert.IsNull(actualJoinTableRecord);
            Assert.IsTrue(CompareAssignments(GetExpectedAssignmentRecords(), actualAssignmentRecords));
        }

        [TestCase(1)]
        public void ShowAvailableRoles_ReturnRolesThatAreNotUsedInProjectExcludingEmployee(int projectId)
        {
            //Arange
            AdministratorService service = new AdministratorService(_unitOfWork, _mapper);

            List<RoleModel> expectedRoles = new List<RoleModel>() {
                new RoleModel{Id = 1, Name = "Employee"},
                new RoleModel { Id = 2, Name = "Manager" } 
            };

            //Act
            var roles = service.ShowAvailableRoles(projectId);

            //Assert
            Assert.IsTrue(CompareRoleModels(expectedRoles, roles));
        }

        [TestCase(1,2,2,null,"Manager","Manager")]
        public async Task SetEmployeeRole_SupplySpecialRole_ChangeRoleAndPosition(int employeeId, int projectId, int roleId, int? positionId,string expectedRole, string expectedPosition)
        {
            //Arrange
            AdministratorService service = new AdministratorService(_unitOfWork, _mapper);

            //Act
            await service.SetEmployeeRoleAsync(employeeId, projectId, roleId);
            var actualEmployeeRoleAndPosition = _unitOfWork.JoinRepository.GetAllWithDetails().FirstOrDefault(p=>p.EmployeeId==employeeId&&p.ProjectId==projectId);

            //Assert
            Assert.AreEqual(expectedRole, actualEmployeeRoleAndPosition.Role.Name);
            Assert.AreEqual(expectedPosition, actualEmployeeRoleAndPosition.Position.PositionName);
        }

        private IEnumerable<Assignment> GetExpectedAssignmentRecords()
        {
            return new List<Assignment>
            {
                new Assignment
                {
                    AssignmentId = 4,
                    AssignmentName = "Choose a dog breed for experiments",
                    AssignmentDescription = "For our goal, we need to select fast, hardy and good looking dogo breed",
                    AssignmentStatus = "Done",
                    EmployeeId = null,// maybe, if the task has status "Done", employeeId shouldn't be deleted?
                    ProjectId = 2,
                    Deadline = DateTime.Now.Date.AddDays(-40)
                },
                new Assignment
                {
                    AssignmentId = 7,
                    AssignmentName = "The dog must be bulletproof (seriously?)",
                    AssignmentDescription = "So... our customer wants the dog to be bulletproof. You've heard it right. And no vests must be used for achievement of the goal.",
                    AssignmentStatus = "No Employee",
                    Deadline = DateTime.Now.Date.AddDays(33),
                    EmployeeId = null,
                    ProjectId = 2
                }
            };

        }
        private bool CompareAssignments(IEnumerable<Assignment> x, IEnumerable<Assignment> y)
        {
            x = x.OrderBy(a => a.AssignmentId);
            y = y.OrderBy(a => a.AssignmentId);

            if (x.Count() != y.Count())
                return false;

            var xArray = x.ToArray();
            var yArray = y.ToArray();

            for(int i=0; i<xArray.Length; i++)
            {
                bool result = xArray[i].AssignmentId == yArray[i].AssignmentId &&
                    xArray[i].AssignmentName == yArray[i].AssignmentName &&
                    xArray[i].AssignmentDescription == yArray[i].AssignmentDescription &&
                    xArray[i].AssignmentStatus == yArray[i].AssignmentStatus &&
                    xArray[i].EmployeeId == yArray[i].EmployeeId &&
                    xArray[i].ProjectId == yArray[i].ProjectId;
                if (result == false)
                    return false;
            }
            return true;
        }

        private bool CompareRoleModels(IEnumerable<RoleModel> x, IEnumerable<RoleModel> y)
        {
            if (x.Count() != y.Count())
                return false;

            var xArray = x.ToArray();
            var yArray = y.ToArray();

            for(int i=0; i<xArray.Length; i++)
            {
                bool equal = xArray[i].Id == yArray[i].Id &&
                    xArray[i].Name == yArray[i].Name;
                if (!equal)
                    return false;
            }
            return true;
        }

    }
}
