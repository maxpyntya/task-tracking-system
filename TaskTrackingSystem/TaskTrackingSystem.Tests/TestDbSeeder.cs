﻿using DAL.Context_Classes;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace TaskTrackingSystem.Tests
{
    public class TestDbSeeder
    {
        public static void SeedDb(TTSContext context)
        {
            context.Projects.AddRange(
                new Project
                {
                    ProjectId = 1,
                    ProjectName = "Arasaka",
                    ProjectDescription = "World's #1 corporation which will enslave humans with newest technology",
                    StartDate = DateTime.Now.Date.AddDays(-20),
                    PercentageOfCompletion = 25
                },
                new Project
                {
                    ProjectId = 2,
                    ProjectName = "Bolt",
                    ProjectDescription = "Genetically modified dog with superpowers. Creation purpose : cuz we can.",
                    StartDate = DateTime.Now.Date.AddDays(-44),
                    PercentageOfCompletion = 50

                },
                new Project
                {
                    ProjectId = 3,
                    ProjectName = "MarketplaceWebsite",
                    ProjectDescription = "The name says it all. It is a marketplace website",
                    StartDate = DateTime.Now.Date.AddDays(-8),
                    PercentageOfCompletion = 66.66
                },
                new Project
                {
                    ProjectId = 4,
                    ProjectName = "ProjectTest4",
                    ProjectDescription = "Description of ProjectTest4",
                    StartDate = DateTime.Now.Date.AddDays(15),
                    PercentageOfCompletion = 0
                },
                new Project
                {
                    ProjectId = 5,
                    ProjectName = "ProjectTest5",
                    ProjectDescription = "Description of ProjectTest5",
                    StartDate = DateTime.Now.Date.AddDays(10),
                    PercentageOfCompletion = 0
                });

            context.Users.AddRange(
                new Employee
                {
                    Id = 1,
                    FirstName = "Johnny",
                    LastName = "Lecter",
                    UserName = "JohnnyLecter1",
                    Email = "me_Lecter@email.com"
                },
                new Employee
                {
                    Id = 2,
                    FirstName = "Gabriel",
                    LastName = "Lincoln",
                    UserName = "GabrielLincoln1",
                    Email = "Not_That_Lincoln@email.com"
                },
                new Employee
                {
                    Id = 3,
                    FirstName = "Linda",
                    LastName = "Svarovsky",
                    UserName = "LovelyBird3",
                    Email = "BlueAraLover@email.com"

                },
                new Employee
                {
                    Id = 4,
                    FirstName = "Mike",
                    LastName = "McGillan",
                    UserName = "SaintMy3rs",
                    Email = "SilentMike1978@email.com"

                },
                new Employee
                {
                    Id = 5,
                    FirstName = "Nina",
                    LastName = "Boykovich",
                    UserName = "HyperN1na",
                    Email = "TheRealNine@email.com"

                },
                new Employee
                {
                    Id = 6,
                    FirstName = "Ashley",
                    LastName = "Cooper",
                    UserName = "HotChebur3k",
                    Email = "CooperIsMyRealName@email.com"
                });
            context.Assignments.AddRange(
                new Assignment
                {
                    AssignmentId = 1,
                    AssignmentName = "Make home page design",
                    AssignmentDescription = "We need cool design. Some more words.",
                    AssignmentStatus = "Done",
                    EmployeeId = 2,
                    ProjectId = 3,
                    Deadline = DateTime.Now.Date.AddDays(-6)
                },
                new Assignment
                {
                    AssignmentId = 2,
                    AssignmentName = "User profile functionality",
                    AssignmentDescription = "Add necessary functionality to profile, like settings, etc.",
                    AssignmentStatus = "In progress",
                    EmployeeId = 3,
                    ProjectId = 3,
                    Deadline = DateTime.Now.Date.AddDays(2)
                },
                new Assignment
                {
                    AssignmentId = 3,
                    AssignmentName = "TF authentication",
                    AssignmentDescription = "Add TF authentication to our system (using email or phone)",
                    AssignmentStatus = "In progress",
                    EmployeeId = 6,
                    ProjectId = 3,
                    Deadline = DateTime.Now.Date.AddDays(10)
                },
                new Assignment
                {
                    AssignmentId = 4,
                    AssignmentName = "Choose a dog breed for experiments",
                    AssignmentDescription = "For our goal, we need to select fast, hardy and good looking dogo breed",
                    AssignmentStatus = "Done",
                    EmployeeId = 1,
                    ProjectId = 2,
                    Deadline = DateTime.Now.Date.AddDays(-40)
                },
                new Assignment
                {
                    AssignmentId = 5,
                    AssignmentName = "Find a way to make dogo superfast",
                    AssignmentDescription = "Our customer wants the dogo to be really, REALLY fast. Find out how we can do that.",
                    AssignmentStatus = "In progress",
                    EmployeeId = 4,
                    ProjectId = 2,
                    Deadline = DateTime.Now.Date.AddDays(20)
                },
                new Assignment
                {
                    AssignmentId = 6,
                    AssignmentName = "Make the dog feel comfortable",
                    AssignmentDescription = "The dog is too nervous and starts to behave aggressively, such behavior interferes with the achievement of the goal. Calm the dog down. ",
                    AssignmentStatus = "Done",
                    EmployeeId = 5,
                    ProjectId = 2,
                    Deadline = DateTime.Now.Date.AddDays(-20)
                },
                new Assignment
                {
                    AssignmentId = 7,
                    AssignmentName = "The dog must be bulletproof (seriously?)",
                    AssignmentDescription = "So... our customer wants the dog to be bulletproof. You've heard it right. And no vests must be used for achievement of the goal.",
                    AssignmentStatus = "In progress",
                    Deadline = DateTime.Now.Date.AddDays(33),
                    EmployeeId = 1,
                    ProjectId = 2
                },
                new Assignment
                {
                    AssignmentId = 8,
                    AssignmentName = "Create technology to control human minds",
                    AssignmentDescription = "We need a technology to control human minds. It must be something that is used by everyone and everywhere, easy to use and addictive",
                    AssignmentStatus = "In progress",
                    Deadline = DateTime.Now.Date.AddDays(25),
                    EmployeeId = 6,
                    ProjectId = 1
                },
                new Assignment
                {
                    AssignmentId = 9,
                    AssignmentName = "Start an advertising campaign",
                    AssignmentDescription = "Our company needs to be seen as last stronghold of kindness to eliminate any suspicions",
                    AssignmentStatus = "Done",
                    Deadline = DateTime.Now.Date.AddDays(-8),
                    EmployeeId = 2,
                    ProjectId = 1
                },
                new Assignment
                {
                    AssignmentId = 10,
                    AssignmentName = "Find rich people with shared interests",
                    AssignmentDescription = "It is better to have a money bag in your back to solve any unexpected problems. So, we need to find greedy rich people which are interested in enslaving people and investing in our company",
                    AssignmentStatus = "In progress",
                    Deadline = DateTime.Now.Date.AddDays(15),
                    EmployeeId = 4,
                    ProjectId = 1
                },
                new Assignment
                {
                    AssignmentId = 11,
                    AssignmentName = "Create offshore bank account in Switzerland",
                    AssignmentDescription = "That's just for... backup, you know. We are not going to steal anything!",
                    AssignmentStatus = "In progress",
                    Deadline = DateTime.Now.Date.AddDays(3),
                    EmployeeId = 3,
                    ProjectId = 1
                },
                new Assignment
                {
                    AssignmentId = 12,
                    AssignmentName = "Make search by name and\\or id field",
                    AssignmentDescription = "Our customers need to find the products somehow, right? So we are going to allow them search them by id and/or name",
                    AssignmentStatus = "In progress",
                    Deadline = DateTime.Now.Date.AddDays(3),
                    EmployeeId = null,
                    ProjectId = 3

                });
            context.Positions.AddRange(
                new Position { PositionId = 1, PositionName = "Minion" },
                new Position { PositionId = 2, PositionName = "Senior Geneticist" },
                new Position { PositionId = 3, PositionName = "Designer" },
                new Position { PositionId = 4, PositionName = "Developer" },
                new Position { PositionId = 5, PositionName = "PR Manager" },
                new Position { PositionId = 6, PositionName = "Financier" },
                new Position { PositionId = 7, PositionName = "Engineer" },
                new Position { PositionId = 8, PositionName = "Jr. Geneticist" },
                new Position { PositionId = 9, PositionName = "HR" },
                new Position { PositionId = 10, PositionName = "Manager" });

            context.Roles.AddRange(
                new Role { Id = 1, Name = "Employee" },
                new Role { Id = 2, Name = "Manager" });

            context.EmployeeProjectPositionRoles.AddRange(
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 1,
                    ProjectId = 2,
                    PositionId = 2,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 2,
                    ProjectId = 3,
                    PositionId = 3,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 2,
                    ProjectId = 1,
                    PositionId = 5,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 3,
                    ProjectId = 3,
                    PositionId = 4,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 3,
                    ProjectId = 1,
                    PositionId = 6,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 4,
                    ProjectId = 2,
                    PositionId = 8,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 4,
                    ProjectId = 1,
                    PositionId = 9,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 5,
                    ProjectId = 2,
                    PositionId = 1,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 6,
                    ProjectId = 3,
                    PositionId = 4,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 6,
                    ProjectId = 1,
                    PositionId = 7,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 2,
                    ProjectId = 4,
                    PositionId = 7,
                    RoleId = 2
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 2,
                    ProjectId = 5,
                    PositionId = 3,
                    RoleId = 1
                });
        }
    }
}
