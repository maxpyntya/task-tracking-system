﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.Services;
using DAL.Interfaces;
using NUnit;
using Moq;
using System.Linq;
using DAL.Models;
using BLL.Models;
using NUnit.Framework;
using System.Threading.Tasks;
using AutoMapper;
using BLL;
using BLL.Interfaces;
using DAL.Context_Classes;
using Microsoft.EntityFrameworkCore;
using DAL;

namespace TaskTrackingSystem.Tests
{
    [TestFixture]
    public class EmployeeServiceTests
    { 
        IMapper mapper;
        TTSContext context;
        IUnitOfWork unitOfWork;

        [OneTimeSetUp]
        public void SetupMapper()
        {
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(new AutomapperProfile()));
            mapper = new Mapper(configuration);
        }

        [SetUp]
        public void SetUpDb()
        {
            DbContextOptions<TTSContext> options = new DbContextOptionsBuilder<TTSContext>().UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            context = new TTSContext(options);
            TestDbSeeder.SeedDb(context);
            unitOfWork = new UnitOfWork(context);
            context.SaveChanges();
        }

        [TearDown]
        public void TearDownDb()
        {
            context.Dispose();
        }
        #region ShowProjects with records where required employee is present, returns associated projects
        [TestCase(2)]
        public void Test1_ShowProjects_EmployeeIsAssignedToProjects_ReturnAssociatedProjects(int employeeId)
        {
            //Arrange
            EmployeeService service = new EmployeeService(unitOfWork, mapper);
            service.UserId = employeeId;

            //Act
            var results = service.ShowProjects(2);

            //Assert
            Assert.IsTrue(Compare_EmployeeAssociatedProjectCollections(Test1_GetDesiredResult(),results));
        }


        private IEnumerable<EmployeeAssociatedProject> Test1_GetDesiredResult()
        {
            return new List<EmployeeAssociatedProject>
            {
                new EmployeeAssociatedProject
                {
                    ProjectId = 1,
                    ProjectName = "Arasaka",
                    ProjectDescription = "World's #1 corporation which will enslave humans with newest technology",
                    CurrentEmployeePosition = "PR Manager",
                    StartDate = DateTime.Now.Date.AddDays(-20),
                    CompletionPercentage = 25
                },
                new EmployeeAssociatedProject
                {
                    ProjectId = 3,
                    ProjectName = "MarketplaceWebsite",
                    ProjectDescription = "The name says it all. It is a marketplace website",
                    CurrentEmployeePosition = "Designer",
                    StartDate = DateTime.Now.Date.AddDays(-8),
                    CompletionPercentage = 66.66
                }
            };
        }

        #endregion


        private bool Compare_EmployeeAssociatedProjectCollections(IEnumerable<EmployeeAssociatedProject> x, IEnumerable<EmployeeAssociatedProject> y)
        {
            if (x.Count() != y.Count())
                return false;
            var xArray = x.OrderBy(i=>i.ProjectId).ToArray();
            var yArray = y.OrderBy(i => i.ProjectId).ToArray();

            for(int i=0; i<x.Count(); i++)
            {
                if (xArray[i].ProjectId != yArray[i].ProjectId)
                    return false;
                if (xArray[i].ProjectName != yArray[i].ProjectName)
                    return false;
                if (xArray[i].ProjectDescription != yArray[i].ProjectDescription)
                    return false;
                if (xArray[i].StartDate != yArray[i].StartDate)
                    return false;
                if (xArray[i].CloseDate != yArray[i].CloseDate)
                    return false;
                if (xArray[i].CompletionPercentage != yArray[i].CompletionPercentage)
                    return false;
                if (xArray[i].CurrentEmployeePosition != yArray[i].CurrentEmployeePosition)
                    return false;
                
            }
            return true;
        }

        [TestCase(2,2,2,2)]
        public void ShowProjects_NotFirstPage_ReturnCorrectAmountOfProjects(int employeeId, int numberOfProjectsPerPage, int pageNumber, int expectedAmountOfProjects)
        {
            //Arrange
            EmployeeService service = new EmployeeService(unitOfWork, mapper);
            service.UserId = employeeId;

            //Act
            var actualProjects = service.ShowProjects(numberOfProjectsPerPage, 2);

            //Assert
            Assert.AreEqual(expectedAmountOfProjects, actualProjects.Count());
            Assert.IsTrue(Compare_EmployeeAssociatedProjectCollections(ShowProjects_NotFirstPage_GetExpetedProjects(), actualProjects));
        }

        private IEnumerable<EmployeeAssociatedProject> ShowProjects_NotFirstPage_GetExpetedProjects()
        {
            return new List<EmployeeAssociatedProject>
            {
                new EmployeeAssociatedProject
                {
                    ProjectId = 4,
                    ProjectName = "ProjectTest4",
                    ProjectDescription = "Description of ProjectTest4",
                    StartDate = DateTime.Now.Date.AddDays(15),
                    CurrentEmployeePosition = "Engineer",
                    CompletionPercentage = 0
                },
                new EmployeeAssociatedProject
                {
                    ProjectId = 5,
                    ProjectName = "ProjectTest5",
                    ProjectDescription = "Description of ProjectTest5",
                    StartDate = DateTime.Now.Date.AddDays(10),
                    CurrentEmployeePosition = "Designer",
                    CompletionPercentage = 0
                }
            };
        }

        [TestCase(1,7,"Done",75)]
        public async Task Test2_SetTaskStatusAsync_SetTaskAsDone_UpdateCompletionPercentage(int employeeId, int taskId, string newStatus, double expectedPercentage)
        {
            //Arrange
            EmployeeService service = new EmployeeService(unitOfWork, mapper);
            service.UserId = employeeId;

            //Act
            await service.SetTaskStatusAsync(taskId, newStatus);
            var actualTask = await unitOfWork.AssignmentRepository.GetByIdAsync(taskId);
            var actualProjectCompletionPercentage = unitOfWork.ProjectRepository.GetAll().FirstOrDefault(p => p.ProjectId == actualTask.ProjectId).PercentageOfCompletion;

            expectedPercentage = Math.Round(expectedPercentage, 2);

            //Assert
            Assert.AreEqual(newStatus, actualTask.AssignmentStatus);
            Assert.AreEqual(expectedPercentage, actualProjectCompletionPercentage);
        }

        [TestCase(1,2,2,1)]
        public async Task GetProjectAsync_ReturnMappedProject(int employeeId, int projectId, int numberOfTasksPerPage, int pageNumber)
        {
            //Arrange
            EmployeeService service = new EmployeeService(unitOfWork, mapper);
            service.UserId = employeeId;

            //Act
            var actualModel = await service.GetProjectAsync(projectId,numberOfTasksPerPage,pageNumber);

            //Assert
            Assert.IsTrue(Compare_ProjectWithTaskModels(GetExpectedProjectWithTasksModel(),actualModel));
        }

        private ProjectWithTasksModel GetExpectedProjectWithTasksModel()
        {
            return new ProjectWithTasksModel()
            {
                ProjectId = 2,
                ProjectName = "Bolt",
                ProjectDescription = "Genetically modified dog with superpowers. Creation purpose : cuz we can.",
                PercentageOfCompletion = 50,
                StartDate = DateTime.Now.Date.AddDays(-44),
                Assignments = new List<AssignmentModel>
                {
                    new AssignmentModel
                    {
                        AssignmentId = 4,
                        AssignmentName = "Choose a dog breed for experiments",
                        AssignmentDescription = "For our goal, we need to select fast, hardy and good looking dogo breed",
                        AssignmentStatus = "Done",
                        Deadline = DateTime.Now.Date.AddDays(-40),
                        ProjectId = 2,
                        EmployeeId = 1
                    },
                    new AssignmentModel
                    {
                        AssignmentId = 7,
                        AssignmentName = "The dog must be bulletproof (seriously?)",
                        AssignmentDescription = "So... our customer wants the dog to be bulletproof. You've heard it right. And no vests must be used for achievement of the goal.",
                        AssignmentStatus = "In progress",
                        Deadline = DateTime.Now.Date.AddDays(33),
                        EmployeeId = 1,
                        ProjectId = 2
                    }
                }
            };
        }

        private bool Compare_ProjectWithTaskModels(ProjectWithTasksModel x, ProjectWithTasksModel y)
        {
            bool projectEqual = x.ProjectId == y.ProjectId &&
                x.ProjectName == y.ProjectName &&
                x.ProjectDescription == y.ProjectDescription &&
                x.PercentageOfCompletion == y.PercentageOfCompletion &&
                x.StartDate == y.StartDate &&
                x.CloseDate == y.CloseDate;

            if (!projectEqual || !(x.Assignments.Count() == y.Assignments.Count()))
                return false;

            var xAssignments = x.Assignments.ToArray();
            var yAssignments = y.Assignments.ToArray();

            for(int i=0; i<xAssignments.Length; i++)
            {
                bool equal = xAssignments[i].AssignmentId == yAssignments[i].AssignmentId &&
                    xAssignments[i].ProjectId == yAssignments[i].ProjectId &&
                    xAssignments[i].EmployeeId == yAssignments[i].EmployeeId &&
                    xAssignments[i].AssignmentName == yAssignments[i].AssignmentName &&
                    xAssignments[i].AssignmentDescription == yAssignments[i].AssignmentDescription &&
                    xAssignments[i].AssignmentStatus == yAssignments[i].AssignmentStatus &&
                    xAssignments[i].Deadline == yAssignments[i].Deadline;
                if (!equal)
                    return false;
            }
            return true;
        }
    }
}
