﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Models
{
    public class Project
    {
        [Key]
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public double PercentageOfCompletion { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? CloseDate { get; set; }


        public ICollection<EmployeeProjectPositionRole> EmployeeProjectPositionsRoles { get; set; }
        public ICollection<Assignment> Assignments { get; set; }

    }
}
