﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Models
{
    public class Position
    {
        [Key]
        public int PositionId { get; set; }
        public string PositionName { get; set; }

        public ICollection<EmployeeProjectPositionRole> EmployeeProjectPositionsRoles { get; set; }

    }
}
