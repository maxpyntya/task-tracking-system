﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class EmployeeProjectPositionRole
    {
        [Key]
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }
        [Key]
        public int ProjectId { get; set; }

        [ForeignKey("ProjectId")]
        public Project Project { get; set; }
        public int PositionId { get; set; }
        [Required]
        [ForeignKey("PositionId")]
        public Position Position { get; set; }
        public int RoleId { get; set; }

        [Required]
        public Role Role { get; set; }

    }
}
