﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Employee : IdentityUser<int>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public ICollection<EmployeeProjectPositionRole> EmployeeProjectPositionsRoles { get; set; }
        public ICollection<Assignment> Assignments { get; set; }

    }
}
