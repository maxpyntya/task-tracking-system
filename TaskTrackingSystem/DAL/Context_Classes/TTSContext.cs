﻿using DAL.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Context_Classes
{
    public class TTSContext : IdentityDbContext<Employee,Role,int>
    {
        public TTSContext(DbContextOptions<TTSContext> options) : base(options)
        {

        }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<EmployeeProjectPositionRole> EmployeeProjectPositionRoles { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);


            builder.Entity<Employee>().Property(e => e.Id).HasColumnName("EmployeeId");
            builder.Entity<Employee>().Property(e => e.FirstName).IsRequired().HasMaxLength(60);
            builder.Entity<Employee>().Property(e => e.LastName).IsRequired().HasMaxLength(60);
            builder.Entity<Employee>().HasIndex(e => e.Email).IsUnique();
            builder.Entity<Employee>().Property(e => e.Email).IsRequired();
            builder.Entity<Employee>().HasIndex(e => e.UserName).IsUnique();
            builder.Entity<Employee>().Property(e => e.UserName).IsRequired();
            builder.Entity<Employee>().ToTable("Employee");

            builder.Entity<Role>().Property(r => r.Id).HasColumnName("RoleId");
            builder.Entity<Role>().Property(r => r.Name).HasColumnName("RoleName");
            builder.Entity<Role>().Property(r => r.Name).IsRequired().HasMaxLength(60);

            builder.Entity<Position>().Property(p => p.PositionName).IsRequired().HasMaxLength(80);

            builder.Entity<Project>().Property(p => p.ProjectName).IsRequired().HasMaxLength(80);
            builder.Entity<Project>().Property(p => p.ProjectDescription).IsRequired();
            builder.Entity<Project>().Property(p => p.StartDate).IsRequired();
            builder.Entity<Project>().Property(p => p.PercentageOfCompletion).IsRequired();

            builder.Entity<Assignment>().Property(a => a.AssignmentName).IsRequired().HasMaxLength(100);
            builder.Entity<Assignment>().Property(a => a.AssignmentDescription).IsRequired();
            builder.Entity<Assignment>().Property(a => a.AssignmentStatus).IsRequired().HasMaxLength(50);
            builder.Entity<Assignment>().Property(a => a.Deadline).IsRequired();
            builder.Entity<Assignment>().HasOne(a => a.Employee).WithMany(e => e.Assignments).HasForeignKey(a => a.EmployeeId);
            builder.Entity<Assignment>().HasOne(a => a.Project).WithMany(p => p.Assignments).HasForeignKey(a => a.ProjectId);

            builder.Entity<EmployeeProjectPositionRole>().HasKey(x => new { x.EmployeeId, x.ProjectId });
            builder.Entity<EmployeeProjectPositionRole>().HasOne(ep => ep.Role).WithMany(r => r.EmployeeProjectPositionsRoles).HasForeignKey(ep => ep.RoleId);
            
            //Seed data

            builder.Entity<Project>().HasData(
                new Project
                {
                    ProjectId = 1,
                    ProjectName = "Arasaka",
                    ProjectDescription = "World's #1 corporation which will enslave humans with newest technology",
                    StartDate = DateTime.Now.AddDays(-20),
                    PercentageOfCompletion = 25
                },
                new Project
                {
                    ProjectId = 2,
                    ProjectName = "Bolt",
                    ProjectDescription = "Genetically modified dog with superpowers. Creation purpose : cuz we can.",
                    StartDate = DateTime.Now.AddDays(-44),
                    PercentageOfCompletion = 50

                },
                new Project
                {
                    ProjectId = 3,
                    ProjectName = "MarketplaceWebsite",
                    ProjectDescription = "The name says it all. It is a marketplace website",
                    StartDate = DateTime.Now.AddDays(-8),
                    PercentageOfCompletion = 66.66
                }
                ); ;
            builder.Entity<Role>().HasData(
                new Role { Id = 1, Name = "Employee" },
                new Role { Id = 2, Name = "Manager" },
                new Role { Id = 3, Name = "Admin"}
                );
            builder.Entity<Assignment>().HasData(
                new Assignment
                {
                    AssignmentId = 1,
                    AssignmentName = "Make home page design",
                    AssignmentDescription = "We need cool design. Some more words.",
                    AssignmentStatus = "Done",
                    EmployeeId = 2,
                    ProjectId = 3,
                    Deadline = DateTime.Now.AddDays(-6)
                },
                new Assignment
                {
                    AssignmentId = 2,
                    AssignmentName = "User profile functionality",
                    AssignmentDescription = "Add necessary functionality to profile, like settings, etc.",
                    AssignmentStatus = "In progress",
                    EmployeeId = 3,
                    ProjectId = 3,
                    Deadline = DateTime.Now.AddDays(2)
                },
                new Assignment
                {
                    AssignmentId = 3,
                    AssignmentName = "TF authentication",
                    AssignmentDescription = "Add TF authentication to our system (using email or phone)",
                    AssignmentStatus = "In progress",
                    EmployeeId = 6,
                    ProjectId = 3,
                    Deadline = DateTime.Now.AddDays(10)
                },
                new Assignment
                {
                    AssignmentId = 4,
                    AssignmentName = "Choose a dog breed for experiments",
                    AssignmentDescription = "For our goal, we need to select fast, hardy and good looking dogo breed",
                    AssignmentStatus = "Done",
                    EmployeeId = 1,
                    ProjectId = 2,
                    Deadline = DateTime.Now.AddDays(-40)
                },
                new Assignment
                {
                    AssignmentId = 5,
                    AssignmentName = "Find a way to make dogo superfast",
                    AssignmentDescription = "Our customer wants the dogo to be really, REALLY fast. Find out how we can do that.",
                    AssignmentStatus = "In progress",
                    EmployeeId = 4,
                    ProjectId = 2,
                    Deadline = DateTime.Now.AddDays(20)
                },
                new Assignment
                {
                    AssignmentId = 6,
                    AssignmentName = "Make the dog feel comfortable",
                    AssignmentDescription = "The dog is too nervous and starts to behave aggressively, such behavior interferes with the achievement of the goal. Calm the dog down. ",
                    AssignmentStatus = "Done",
                    EmployeeId = 5,
                    ProjectId = 2,
                    Deadline = DateTime.Now.AddDays(-20)
                },
                new Assignment
                {
                    AssignmentId = 7,
                    AssignmentName = "The dog must be bulletproof (seriously?)",
                    AssignmentDescription = "So... our customer wants the dog to be bulletproof. You've heard it right. And no vests must be used for achievement of the goal.",
                    AssignmentStatus = "In progress",
                    Deadline = DateTime.Now.AddDays(33),
                    EmployeeId = 1,
                    ProjectId = 2
                },
                new Assignment
                {
                    AssignmentId = 8,
                    AssignmentName = "Create technology to control human minds",
                    AssignmentDescription = "We need a technology to control human minds. It must be something that is used by everyone and everywhere, easy to use and addictive",
                    AssignmentStatus = "In progress",
                    Deadline = DateTime.Now.AddDays(25),
                    EmployeeId = 6,
                    ProjectId = 1
                },
                new Assignment
                {
                    AssignmentId = 9,
                    AssignmentName = "Start an advertising campaign",
                    AssignmentDescription = "Our company needs to be seen as last stronghold of kindness to eliminate any suspicions",
                    AssignmentStatus = "Done",
                    Deadline = DateTime.Now.AddDays(-8),
                    EmployeeId = 2,
                    ProjectId = 1
                },
                new Assignment
                {
                    AssignmentId = 10,
                    AssignmentName = "Find rich people with shared interests",
                    AssignmentDescription = "It is better to have a money bag in your back to solve any unexpected problems. So, we need to find greedy rich people which are interested in enslaving people and investing in our company",
                    AssignmentStatus = "In progress",
                    Deadline = DateTime.Now.AddDays(15),
                    EmployeeId = 4,
                    ProjectId = 1
                },
                new Assignment
                {
                    AssignmentId = 11,
                    AssignmentName = "Create offshore bank account in Switzerland",
                    AssignmentDescription = "That's just for... backup, you know. We are not going to steal anything!",
                    AssignmentStatus = "In progress",
                    Deadline = DateTime.Now.AddDays(3),
                    EmployeeId = 3,
                    ProjectId = 1
                }
                );

            builder.Entity<Position>().HasData(
                new Position { PositionId = 1, PositionName = "Minion" },
                new Position { PositionId = 2, PositionName = "Senior Geneticist" },
                new Position { PositionId = 3, PositionName = "Designer" },
                new Position { PositionId = 4, PositionName = "Developer" },
                new Position { PositionId = 5, PositionName = "PR Manager" },
                new Position { PositionId = 6, PositionName = "Financier" },
                new Position { PositionId = 7, PositionName = "Engineer" },
                new Position { PositionId = 8, PositionName = "Jr. Geneticist" },
                new Position { PositionId = 9, PositionName = "HR" },
                new Position { PositionId = 10, PositionName = "Manager"}
                );

            builder.Entity<EmployeeProjectPositionRole>().HasData(
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 1,
                    ProjectId = 2,
                    PositionId = 2,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 2,
                    ProjectId = 3,
                    PositionId = 3,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 2,
                    ProjectId = 1,
                    PositionId = 5,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 3,
                    ProjectId = 3,
                    PositionId = 4,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 3,
                    ProjectId = 1,
                    PositionId = 6,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 4,
                    ProjectId = 2,
                    PositionId = 8,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 4,
                    ProjectId = 1,
                    PositionId = 9,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 5,
                    ProjectId = 2,
                    PositionId = 1,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 6,
                    ProjectId = 3,
                    PositionId = 4,
                    RoleId = 1
                },
                new EmployeeProjectPositionRole
                {
                    EmployeeId = 6,
                    ProjectId = 1,
                    PositionId = 7,
                    RoleId = 1
                }
                );
        }
    }
}
