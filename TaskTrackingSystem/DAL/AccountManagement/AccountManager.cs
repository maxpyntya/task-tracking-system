﻿using DAL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NamedConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.AccountManagement
{
    public class AccountManager : SignInManager<Employee>, IAuthentication<Employee>
    {
        private bool disposedValue;

        public AccountManager(UserManager<Employee> userManager, IHttpContextAccessor contextAccessor, IUserClaimsPrincipalFactory<Employee> claimsFactory, IOptions<IdentityOptions> optionsAccessor, ILogger<SignInManager<Employee>> logger, IAuthenticationSchemeProvider schemes, IUserConfirmation<Employee> confirmation) : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes, confirmation)
        {
        }

        public async virtual Task<UserAccountOperationResult> CreateUserAsync(Employee user, string password)
        {
            var result = await this.UserManager.CreateAsync(user, password);
            if(result != IdentityResult.Success)
            {
                var errorMessage = string.Join("\n",result.Errors.Select(e => e.Description));
                return new UserAccountOperationResult(OperationResultEnum.Failed, errorMessage);

            }
            return new UserAccountOperationResult(OperationResultEnum.OK);            
        }

        public async Task<UserAccountOperationResult> PasswordSignInAsync(Employee user, string password)
        {
            var signInResult = await this.PasswordSignInAsync(user, password, false, false);
            if (signInResult != SignInResult.Success)
                return new UserAccountOperationResult(OperationResultEnum.Failed, "Invalid credentials!");
            var operationResult = new UserAccountOperationResult(OperationResultEnum.OK, await GetUserInfoForJwtAsync(user));
            return operationResult;
        }

        private async Task<Dictionary<string,string>> GetUserInfoForJwtAsync(Employee employee)
        {
            Dictionary<string, string> info = new Dictionary<string, string>();
            info.Add("Id", employee.Id.ToString());
            var isAdmin = await UserManager.GetRolesAsync(employee);
            if (isAdmin.Contains("Admin"))
            {
                info.Add("Roles", "Admin");
                return info;
            }
            var roles = UserManager.Users.Include(u => u.EmployeeProjectPositionsRoles).ThenInclude(u=>u.Role)
                .FirstOrDefault(e => e.Id == employee.Id).EmployeeProjectPositionsRoles.Select(r => r.Role.Name).Distinct();
            info.Add("Roles", string.Join(',', roles.Select(r => r)));
            return info;
        }
        public async Task<Employee> FindByUsernameAsync(string username)
        {
            var user = await UserManager.FindByNameAsync(username);
            return user;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    UserManager.Dispose();
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~AccountManager()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

    }
}
