﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IJoinRepository : IRepositoryCommon<EmployeeProjectPositionRole>, IRepositoryCompositeKey<EmployeeProjectPositionRole> ,IAddToDb<EmployeeProjectPositionRole>
    {
        Task<EmployeeProjectPositionRole> GetByIdWithDetailsAsync(int employeeId, int projectId);
        IQueryable<EmployeeProjectPositionRole> GetAllWithDetails();
    }
}
