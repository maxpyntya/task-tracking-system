﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface IPositionRepository : IRepositoryCommon<Position>, IRepositorySingleKey<Position>, IAddToDb<Position>
    {
    }
}
