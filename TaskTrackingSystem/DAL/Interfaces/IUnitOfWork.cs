﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IEmployeeRepository EmployeeRepository { get; set; }
        IProjectRepository ProjectRepository { get; set; }
        IAssignmentRepository AssignmentRepository { get; set; }
        IPositionRepository PositionRepository { get; set; }
        IJoinRepository JoinRepository { get; set; }
        IRoleRepository RoleRepository { get; set; }

        public Task SaveChangesAsync();
    }
}
