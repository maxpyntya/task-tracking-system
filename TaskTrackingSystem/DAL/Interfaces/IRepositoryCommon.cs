﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepositoryCommon<T>
    {
        IQueryable<T> GetAll();
        void UpdateParticularField(T item, string propertyToUpdate);
        void Update(T item);
    }
}
