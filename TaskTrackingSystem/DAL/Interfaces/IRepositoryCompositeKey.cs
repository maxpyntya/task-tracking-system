﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepositoryCompositeKey<T>
    {
        Task<T> GetByIdAsync(params int[] id);
        Task DeleteByIdAsync(params int[] id);

    }
}
