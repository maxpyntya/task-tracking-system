﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface IRoleRepository : IRepositoryCommon<Role>, IRepositorySingleKey<Role>
    {
    }
}
