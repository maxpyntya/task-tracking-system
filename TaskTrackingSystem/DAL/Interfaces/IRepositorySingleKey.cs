﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepositorySingleKey<T>
    { 
        Task<T> GetByIdAsync(int id);
        Task DeleteByIdAsync(int id);

    }
}
