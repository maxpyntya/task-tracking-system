﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Interfaces
{
    public interface IEmployeeRepository : IRepositoryCommon<Employee>, IRepositorySingleKey<Employee>
    {
        IQueryable<Employee> FindEmployeesByName(string firstName, string lastName);
    }
}
