﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NamedConstants;

namespace DAL.Interfaces
{
    public interface IAuthentication<TUser> : IDisposable
    {
        Task<UserAccountOperationResult> CreateUserAsync(TUser user, string password);
        Task<UserAccountOperationResult> PasswordSignInAsync(TUser user, string password);
        Task<TUser> FindByUsernameAsync(string username);
    }
}
