﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IAssignmentRepository : IRepositoryCommon<Assignment>, IRepositorySingleKey<Assignment>, IAddToDb<Assignment>
    {
        IQueryable<Assignment> GetAllWithDetails();
        Task<Assignment> GetByIdWithDetailsAsync(int id);
    }
}
