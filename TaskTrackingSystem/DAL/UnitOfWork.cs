﻿using DAL.Context_Classes;
using DAL.Interfaces;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        TTSContext _context;
        private bool disposedValue;

        public UnitOfWork(TTSContext context)
        {
            _context = context;

            ProjectRepository = new ProjectRepository(context);
            AssignmentRepository = new AssignmentRepository(context);
            PositionRepository = new PositionRepository(context);
            JoinRepository = new JoinRepository(context);
            EmployeeRepository = new EmployeeRepository(context);
            RoleRepository = new RoleRepository(context);
        }
        public IProjectRepository ProjectRepository { get; set; }
        public IAssignmentRepository AssignmentRepository { get; set; }
        public IPositionRepository PositionRepository { get; set ; }
        public IJoinRepository JoinRepository { get; set; }
        public IEmployeeRepository EmployeeRepository { get; set ; }
        public IRoleRepository RoleRepository { get; set ; }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _context.Dispose();
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~UnitOfWork()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
