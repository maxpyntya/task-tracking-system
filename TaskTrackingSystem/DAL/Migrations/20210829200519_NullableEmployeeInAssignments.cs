﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class NullableEmployeeInAssignments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assignments_Employee_EmployeeId",
                table: "Assignments");

            migrationBuilder.AlterColumn<int>(
                name: "EmployeeId",
                table: "Assignments",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "RoleId",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "d8fdc973-2c10-463b-a450-33ffa5edb77d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "RoleId",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "6f7be18e-b5a3-437b-8cf5-50559e7329a3");

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 1,
                column: "Deadline",
                value: new DateTime(2021, 8, 23, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(4592));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 2,
                column: "Deadline",
                value: new DateTime(2021, 8, 31, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5040));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 3,
                column: "Deadline",
                value: new DateTime(2021, 9, 8, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5067));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 4,
                column: "Deadline",
                value: new DateTime(2021, 7, 20, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5075));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 5,
                column: "Deadline",
                value: new DateTime(2021, 9, 18, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5082));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 6,
                column: "Deadline",
                value: new DateTime(2021, 8, 9, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5087));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 7,
                column: "Deadline",
                value: new DateTime(2021, 10, 1, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5092));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 8,
                column: "Deadline",
                value: new DateTime(2021, 9, 23, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5096));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 9,
                column: "Deadline",
                value: new DateTime(2021, 8, 21, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5101));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 10,
                column: "Deadline",
                value: new DateTime(2021, 9, 13, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5106));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 11,
                column: "Deadline",
                value: new DateTime(2021, 9, 1, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5110));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 1,
                column: "StartDate",
                value: new DateTime(2021, 8, 9, 23, 5, 18, 283, DateTimeKind.Local).AddTicks(2793));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 2,
                column: "StartDate",
                value: new DateTime(2021, 7, 16, 23, 5, 18, 290, DateTimeKind.Local).AddTicks(8633));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 3,
                column: "StartDate",
                value: new DateTime(2021, 8, 21, 23, 5, 18, 290, DateTimeKind.Local).AddTicks(8684));

            migrationBuilder.AddForeignKey(
                name: "FK_Assignments_Employee_EmployeeId",
                table: "Assignments",
                column: "EmployeeId",
                principalTable: "Employee",
                principalColumn: "EmployeeId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assignments_Employee_EmployeeId",
                table: "Assignments");

            migrationBuilder.AlterColumn<int>(
                name: "EmployeeId",
                table: "Assignments",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "RoleId",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "d60678dc-198b-4b02-9837-8c66d9084cbf");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "RoleId",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "1ad5bad5-a648-4c4c-b102-33558b76a350");

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 1,
                column: "Deadline",
                value: new DateTime(2021, 8, 22, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(6447));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 2,
                column: "Deadline",
                value: new DateTime(2021, 8, 30, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(6952));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 3,
                column: "Deadline",
                value: new DateTime(2021, 9, 7, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(6983));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 4,
                column: "Deadline",
                value: new DateTime(2021, 7, 19, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(6989));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 5,
                column: "Deadline",
                value: new DateTime(2021, 9, 17, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7055));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 6,
                column: "Deadline",
                value: new DateTime(2021, 8, 8, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7062));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 7,
                column: "Deadline",
                value: new DateTime(2021, 9, 30, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7068));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 8,
                column: "Deadline",
                value: new DateTime(2021, 9, 22, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7072));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 9,
                column: "Deadline",
                value: new DateTime(2021, 8, 20, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7076));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 10,
                column: "Deadline",
                value: new DateTime(2021, 9, 12, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7080));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 11,
                column: "Deadline",
                value: new DateTime(2021, 8, 31, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7085));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 1,
                column: "StartDate",
                value: new DateTime(2021, 8, 8, 15, 41, 3, 391, DateTimeKind.Local).AddTicks(4493));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 2,
                column: "StartDate",
                value: new DateTime(2021, 7, 15, 15, 41, 3, 394, DateTimeKind.Local).AddTicks(1314));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 3,
                column: "StartDate",
                value: new DateTime(2021, 8, 20, 15, 41, 3, 394, DateTimeKind.Local).AddTicks(1362));

            migrationBuilder.AddForeignKey(
                name: "FK_Assignments_Employee_EmployeeId",
                table: "Assignments",
                column: "EmployeeId",
                principalTable: "Employee",
                principalColumn: "EmployeeId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
