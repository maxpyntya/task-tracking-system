﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class AddAdminRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "RoleId",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "10610127-6ecc-452b-a1f2-a6c6ebd834f7");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "RoleId",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "1afe25ad-8e02-4662-853f-c40c29cba106");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "RoleId", "ConcurrencyStamp", "RoleName", "NormalizedName" },
                values: new object[] { 3, "de75a6bf-6384-4ab7-8988-e44c057b2034", "Admin", null });

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 1,
                column: "Deadline",
                value: new DateTime(2021, 8, 25, 15, 59, 22, 556, DateTimeKind.Local).AddTicks(2910));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 2,
                column: "Deadline",
                value: new DateTime(2021, 9, 2, 15, 59, 22, 556, DateTimeKind.Local).AddTicks(3423));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 3,
                column: "Deadline",
                value: new DateTime(2021, 9, 10, 15, 59, 22, 556, DateTimeKind.Local).AddTicks(3452));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 4,
                column: "Deadline",
                value: new DateTime(2021, 7, 22, 15, 59, 22, 556, DateTimeKind.Local).AddTicks(3459));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 5,
                column: "Deadline",
                value: new DateTime(2021, 9, 20, 15, 59, 22, 556, DateTimeKind.Local).AddTicks(3464));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 6,
                column: "Deadline",
                value: new DateTime(2021, 8, 11, 15, 59, 22, 556, DateTimeKind.Local).AddTicks(3468));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 7,
                column: "Deadline",
                value: new DateTime(2021, 10, 3, 15, 59, 22, 556, DateTimeKind.Local).AddTicks(3474));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 8,
                column: "Deadline",
                value: new DateTime(2021, 9, 25, 15, 59, 22, 556, DateTimeKind.Local).AddTicks(3479));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 9,
                column: "Deadline",
                value: new DateTime(2021, 8, 23, 15, 59, 22, 556, DateTimeKind.Local).AddTicks(3485));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 10,
                column: "Deadline",
                value: new DateTime(2021, 9, 15, 15, 59, 22, 556, DateTimeKind.Local).AddTicks(3489));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 11,
                column: "Deadline",
                value: new DateTime(2021, 9, 3, 15, 59, 22, 556, DateTimeKind.Local).AddTicks(3494));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 1,
                column: "StartDate",
                value: new DateTime(2021, 8, 11, 15, 59, 22, 550, DateTimeKind.Local).AddTicks(7305));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 2,
                column: "StartDate",
                value: new DateTime(2021, 7, 18, 15, 59, 22, 553, DateTimeKind.Local).AddTicks(9743));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 3,
                column: "StartDate",
                value: new DateTime(2021, 8, 23, 15, 59, 22, 553, DateTimeKind.Local).AddTicks(9799));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "RoleId",
                keyValue: 3);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "RoleId",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "d8fdc973-2c10-463b-a450-33ffa5edb77d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "RoleId",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "6f7be18e-b5a3-437b-8cf5-50559e7329a3");

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 1,
                column: "Deadline",
                value: new DateTime(2021, 8, 23, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(4592));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 2,
                column: "Deadline",
                value: new DateTime(2021, 8, 31, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5040));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 3,
                column: "Deadline",
                value: new DateTime(2021, 9, 8, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5067));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 4,
                column: "Deadline",
                value: new DateTime(2021, 7, 20, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5075));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 5,
                column: "Deadline",
                value: new DateTime(2021, 9, 18, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5082));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 6,
                column: "Deadline",
                value: new DateTime(2021, 8, 9, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5087));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 7,
                column: "Deadline",
                value: new DateTime(2021, 10, 1, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5092));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 8,
                column: "Deadline",
                value: new DateTime(2021, 9, 23, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5096));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 9,
                column: "Deadline",
                value: new DateTime(2021, 8, 21, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5101));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 10,
                column: "Deadline",
                value: new DateTime(2021, 9, 13, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5106));

            migrationBuilder.UpdateData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 11,
                column: "Deadline",
                value: new DateTime(2021, 9, 1, 23, 5, 18, 292, DateTimeKind.Local).AddTicks(5110));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 1,
                column: "StartDate",
                value: new DateTime(2021, 8, 9, 23, 5, 18, 283, DateTimeKind.Local).AddTicks(2793));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 2,
                column: "StartDate",
                value: new DateTime(2021, 7, 16, 23, 5, 18, 290, DateTimeKind.Local).AddTicks(8633));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 3,
                column: "StartDate",
                value: new DateTime(2021, 8, 21, 23, 5, 18, 290, DateTimeKind.Local).AddTicks(8684));
        }
    }
}
