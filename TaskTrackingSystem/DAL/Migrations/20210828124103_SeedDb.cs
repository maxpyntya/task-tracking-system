﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class SeedDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "RoleId", "ConcurrencyStamp", "RoleName", "NormalizedName" },
                values: new object[,]
                {
                    { 1, "d60678dc-198b-4b02-9837-8c66d9084cbf", "Employee", null },
                    { 2, "1ad5bad5-a648-4c4c-b102-33558b76a350", "Manager", null }
                });

            migrationBuilder.InsertData(
                table: "Positions",
                columns: new[] { "PositionId", "PositionName" },
                values: new object[,]
                {
                    { 1, "Minion" },
                    { 2, "Senior Geneticist" },
                    { 3, "Designer" },
                    { 4, "Developer" },
                    { 5, "PR Manager" },
                    { 6, "Financier" },
                    { 7, "Engineer" },
                    { 8, "Jr. Geneticist" },
                    { 9, "HR" }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "ProjectId", "CloseDate", "PercentageOfCompletion", "ProjectDescription", "ProjectName", "StartDate" },
                values: new object[,]
                {
                    { 1, null, 25.0, "World's #1 corporation which will enslave humans with newest technology", "Arasaka", new DateTime(2021, 8, 8, 15, 41, 3, 391, DateTimeKind.Local).AddTicks(4493) },
                    { 2, null, 50.0, "Genetically modified dog with superpowers. Creation purpose : cuz we can.", "Bolt", new DateTime(2021, 7, 15, 15, 41, 3, 394, DateTimeKind.Local).AddTicks(1314) },
                    { 3, null, 66.659999999999997, "The name says it all. It is a marketplace website", "MarketplaceWebsite", new DateTime(2021, 8, 20, 15, 41, 3, 394, DateTimeKind.Local).AddTicks(1362) }
                });

            migrationBuilder.InsertData(
                table: "Assignments",
                columns: new[] { "AssignmentId", "AssignmentDescription", "AssignmentName", "AssignmentStatus", "Deadline", "EmployeeId", "ProjectId" },
                values: new object[,]
                {
                    { 8, "We need a technology to control human minds. It must be something that is used by everyone and everywhere, easy to use and addictive", "Create technology to control human minds", "In progress", new DateTime(2021, 9, 22, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7072), 6, 1 },
                    { 2, "Add necessary functionality to profile, like settings, etc.", "User profile functionality", "In progress", new DateTime(2021, 8, 30, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(6952), 3, 3 },
                    { 1, "We need cool design. Some more words.", "Make home page design", "Done", new DateTime(2021, 8, 22, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(6447), 2, 3 },
                    { 7, "So... our customer wants the dog to be bulletproof. You've heard it right. And no vests must be used for achievement of the goal.", "The dog must be bulletproof (seriously?)", "In progress", new DateTime(2021, 9, 30, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7068), 1, 2 },
                    { 6, "The dog is too nervous and starts to behave aggressively, such behavior interferes with the achievement of the goal. Calm the dog down. ", "Make the dog feel comfortable", "Done", new DateTime(2021, 8, 8, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7062), 5, 2 },
                    { 3, "Add TF authentication to our system (using email or phone)", "TF authentication", "In progress", new DateTime(2021, 9, 7, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(6983), 6, 3 },
                    { 4, "For our goal, we need to select fast, hardy and good looking dogo breed", "Choose a dog breed for experiments", "Done", new DateTime(2021, 7, 19, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(6989), 1, 2 },
                    { 11, "That's just for... backup, you know. We are not going to steal anything!", "Create offshore bank account in Switzerland", "In progress", new DateTime(2021, 8, 31, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7085), 3, 1 },
                    { 10, "It is better to have a money bag in your back to solve any unexpected problems. So, we need to find greedy rich people which are interested in enslaving people and investing in our company", "Find rich people with shared interests", "In progress", new DateTime(2021, 9, 12, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7080), 4, 1 },
                    { 9, "Our company needs to be seen as last stronghold of kindness to eliminate any suspicions", "Start an advertising campaign", "Done", new DateTime(2021, 8, 20, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7076), 2, 1 },
                    { 5, "Our customer wants the dogo to be really, REALLY fast. Find out how we can do that.", "Find a way to make dogo superfast", "In progress", new DateTime(2021, 9, 17, 15, 41, 3, 395, DateTimeKind.Local).AddTicks(7055), 4, 2 }
                });

            migrationBuilder.InsertData(
                table: "EmployeeProjectPositionRoles",
                columns: new[] { "EmployeeId", "ProjectId", "PositionId", "RoleId" },
                values: new object[,]
                {
                    { 6, 3, 4, 1 },
                    { 1, 2, 2, 1 },
                    { 2, 3, 3, 1 },
                    { 2, 1, 5, 1 },
                    { 3, 3, 4, 1 },
                    { 3, 1, 6, 1 },
                    { 4, 2, 8, 1 },
                    { 4, 1, 9, 1 },
                    { 5, 2, 1, 1 },
                    { 6, 1, 7, 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "RoleId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Assignments",
                keyColumn: "AssignmentId",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "EmployeeProjectPositionRoles",
                keyColumns: new[] { "EmployeeId", "ProjectId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "EmployeeProjectPositionRoles",
                keyColumns: new[] { "EmployeeId", "ProjectId" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "EmployeeProjectPositionRoles",
                keyColumns: new[] { "EmployeeId", "ProjectId" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "EmployeeProjectPositionRoles",
                keyColumns: new[] { "EmployeeId", "ProjectId" },
                keyValues: new object[] { 3, 1 });

            migrationBuilder.DeleteData(
                table: "EmployeeProjectPositionRoles",
                keyColumns: new[] { "EmployeeId", "ProjectId" },
                keyValues: new object[] { 3, 3 });

            migrationBuilder.DeleteData(
                table: "EmployeeProjectPositionRoles",
                keyColumns: new[] { "EmployeeId", "ProjectId" },
                keyValues: new object[] { 4, 1 });

            migrationBuilder.DeleteData(
                table: "EmployeeProjectPositionRoles",
                keyColumns: new[] { "EmployeeId", "ProjectId" },
                keyValues: new object[] { 4, 2 });

            migrationBuilder.DeleteData(
                table: "EmployeeProjectPositionRoles",
                keyColumns: new[] { "EmployeeId", "ProjectId" },
                keyValues: new object[] { 5, 2 });

            migrationBuilder.DeleteData(
                table: "EmployeeProjectPositionRoles",
                keyColumns: new[] { "EmployeeId", "ProjectId" },
                keyValues: new object[] { 6, 1 });

            migrationBuilder.DeleteData(
                table: "EmployeeProjectPositionRoles",
                keyColumns: new[] { "EmployeeId", "ProjectId" },
                keyValues: new object[] { 6, 3 });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "RoleId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "PositionId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "PositionId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "PositionId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "PositionId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "PositionId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "PositionId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "PositionId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "PositionId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Positions",
                keyColumn: "PositionId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "ProjectId",
                keyValue: 3);
        }
    }
}
