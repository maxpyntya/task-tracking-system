﻿using DAL.Context_Classes;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    { 
        TTSContext _context;

        public EmployeeRepository(TTSContext context)
        {
            _context = context;
        }

        public async Task DeleteByIdAsync(int id)
        {
            var employee = await _context.Users.FindAsync(id);
            _context.Users.Remove(employee);
        }

        public IQueryable<Employee> FindEmployeesByName(string firstName, string lastName)
        {
            return _context.Users.Where(u => u.FirstName.Contains(firstName) && u.LastName.Contains(lastName));
        }

        public IQueryable<Employee> GetAll()
        {
            return _context.Users;
        }

        public async Task<Employee> GetByIdAsync(int id)
        {
            var employee = await _context.Users.FindAsync(id);
            return employee;
        }

        public void Update(Employee item)
        {
            _context.Users.Update(item);
        }

        public void UpdateParticularField(Employee item, string propertyToUpdate)
        {
            var entity = _context.ChangeTracker.Entries<Employee>().FirstOrDefault(i => i.Entity.Id == item.Id);
            if (entity != null)
                _context.Users.Attach(item);
            _context.Entry(item).Property(propertyToUpdate).IsModified = true;
        }
    }
}
