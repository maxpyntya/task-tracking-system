﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using System.Threading.Tasks;
using System.Linq;
using DAL.Context_Classes;

namespace DAL.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        TTSContext _context;
        public RoleRepository(TTSContext context)
        {
            _context = context;
        }
        public async Task DeleteByIdAsync(int id)
        {
            var role = await _context.Roles.FindAsync(id);
            _context.Remove(role);
        }

        public IQueryable<Role> GetAll()
        {
            return _context.Roles;
        }

        public async Task<Role> GetByIdAsync(int id)
        {
            var role = await _context.Roles.FindAsync(id);
            return role;
        }

        public void Update(Role item)
        {
            _context.Roles.Update(item);
        }

        public void UpdateParticularField(Role item, string propertyToUpdate)
        {
            var entry = _context.ChangeTracker.Entries<Role>().FirstOrDefault(i => i.Entity.Id == item.Id);
            if (entry != null)
                _context.Roles.Attach(item);
            _context.Entry(item).Property(propertyToUpdate).IsModified = true;
        }
    }
}
