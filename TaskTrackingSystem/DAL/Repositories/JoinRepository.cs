﻿using DAL.Context_Classes;
using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class JoinRepository : IJoinRepository
    {
        TTSContext _context;
        public JoinRepository(TTSContext context)
        {
            _context = context; 
        }
        public async Task AddAsync(EmployeeProjectPositionRole item)
        {
            await _context.EmployeeProjectPositionRoles.AddAsync(item);
        }

        public async Task DeleteByIdAsync(params int[] id)
        {
            if (id.Length < 2)
                throw new ArgumentException("Not all necessary parameters were supplied to delete the entry");
            int employeeId = id[0];
            int projectId = id[1];
            var join = await _context.EmployeeProjectPositionRoles.FindAsync(employeeId,projectId);
            _context.Remove(join);
        }

        public IQueryable<EmployeeProjectPositionRole> GetAll()
        {
            return _context.EmployeeProjectPositionRoles;
        }

        public IQueryable<EmployeeProjectPositionRole> GetAllWithDetails()
        {
            return _context.EmployeeProjectPositionRoles.Include(e => e.Employee).Include(p => p.Position)
                .Include(p => p.Project).Include(r => r.Role);
        }

        public async Task<EmployeeProjectPositionRole> GetByIdAsync(params int[] id)
        {
            if (id.Length < 2)
                throw new ArgumentException("Not all necessary parameters were supplied to retirieve the entry");
            var employeeId = id[0];
            var projectId = id[1];
            var join = await _context.EmployeeProjectPositionRoles.FindAsync(employeeId, projectId);
            return join;
        }

        public async Task<EmployeeProjectPositionRole> GetByIdWithDetailsAsync(int employeeId, int projectId)
        {
            var entry = await _context.EmployeeProjectPositionRoles.FindAsync(employeeId, projectId);
            await _context.Entry(entry).Reference(j => j.Employee).LoadAsync();
            await _context.Entry(entry).Reference(j => j.Project).LoadAsync();
            await _context.Entry(entry).Reference(j => j.Position).LoadAsync();
            await _context.Entry(entry).Reference(j => j.Role).LoadAsync();
            return entry;
        }

        public void Update(EmployeeProjectPositionRole item)
        {
            _context.EmployeeProjectPositionRoles.Update(item);
        }

        public void UpdateParticularField(EmployeeProjectPositionRole item, string propertyToUpdate)
        {
            var entity = _context.ChangeTracker.Entries<EmployeeProjectPositionRole>().FirstOrDefault(i => i.Entity.EmployeeId == item.EmployeeId&&i.Entity.ProjectId==item.ProjectId);
            if (entity != null)
                _context.EmployeeProjectPositionRoles.Attach(item);
            _context.Entry(item).Property(propertyToUpdate).IsModified = true;
        }
    }
}
