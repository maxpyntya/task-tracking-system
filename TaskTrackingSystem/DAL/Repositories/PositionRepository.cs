﻿using DAL.Context_Classes;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class PositionRepository : IPositionRepository
    {
        TTSContext _context;
        public PositionRepository(TTSContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Position item)
        {
            await _context.Positions.AddAsync(item);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var position = await _context.Positions.FindAsync(id);
            _context.Remove(position);
        }

        public IQueryable<Position> GetAll()
        {
            return _context.Positions;
        }

        public async Task<Position> GetByIdAsync(int id)
        {
            return await _context.Positions.FindAsync(id);
        }

        public void Update(Position item)
        {
            _context.Positions.Update(item);
        }

        public void UpdateParticularField(Position item, string propertyToUpdate)
        {
            var entity = _context.ChangeTracker.Entries<Position>().FirstOrDefault(i => i.Entity.PositionId == item.PositionId);
            if (entity != null)
                _context.Positions.Attach(item);
            _context.Entry(item).Property(propertyToUpdate).IsModified = true;
        }
    }
}
