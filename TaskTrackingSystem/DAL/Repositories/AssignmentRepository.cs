﻿using DAL.Context_Classes;
using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class AssignmentRepository : IAssignmentRepository
    {
        TTSContext _context;

        public AssignmentRepository(TTSContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Assignment item)
        {
            await _context.Assignments.AddAsync(item);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var assignment = await _context.Assignments.FindAsync(id);
            _context.Remove(assignment);
        }

        public IQueryable<Assignment> GetAll()
        {
            return _context.Assignments.AsQueryable();
        }

        public IQueryable<Assignment> GetAllWithDetails()
        {
            return _context.Assignments.Include(a => a.Employee).Include(a => a.Project).AsQueryable();
        }

        public async Task<Assignment> GetByIdAsync(int id)
        {
            return await _context.Assignments.FindAsync(id);
        }

        public async Task<Assignment> GetByIdWithDetailsAsync(int id)
        {
            var assignment = await _context.Assignments.FindAsync();
            await _context.Entry(assignment).Reference(a => a.Employee).LoadAsync();
            await _context.Entry(assignment).Reference(a => a.Project).LoadAsync();
            return assignment;
        }

        public void Update(Assignment item)
        {
            _context.Assignments.Update(item);
        }

        public void UpdateParticularField(Assignment item, string propertyToUpdate)
        {
            var entity = _context.ChangeTracker.Entries<Assignment>().FirstOrDefault(i => i.Entity.AssignmentId == item.AssignmentId);
            if (entity != null)
                _context.Assignments.Attach(item);
            _context.Entry(item).Property(propertyToUpdate).IsModified = true;
        }
    }
}
