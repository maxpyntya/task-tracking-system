﻿using DAL.Context_Classes;
using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        TTSContext _context;
        public ProjectRepository(TTSContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Project item)
        {
            await _context.Projects.AddAsync(item);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var project = await _context.Projects.FindAsync(id);
            _context.Remove(project);
        }

        public IQueryable<Project> GetAll()
        {
            return _context.Projects.AsQueryable();
        }

        public IQueryable<Project> GetAllWithDetails()
        {
            return _context.Projects.Include(p => p.Assignments).Include(p=>p.EmployeeProjectPositionsRoles).AsQueryable();
        }

        public async Task<Project> GetByIdAsync(int id)
        {
            return await _context.Projects.FindAsync(id);
        }

        public async Task<Project> GetByIdWithDetailsAsync(int id)
        {
            var project = await _context.Projects.FindAsync(id);
            await _context.Entry(project).Collection(p => p.Assignments).LoadAsync();
            await _context.Entry(project).Collection(p => p.EmployeeProjectPositionsRoles).LoadAsync();
            return project;
        }

        public void Update(Project item)
        {
            _context.Update(item);
        }

        public void UpdateParticularField(Project item, string propertyToUpdate)
        {
            var entity =_context.ChangeTracker.Entries<Project>().FirstOrDefault(i => i.Entity.ProjectId == item.ProjectId);
            if(entity!=null)
                _context.Projects.Attach(item);
            _context.Entry(item).Property(propertyToUpdate).IsModified = true;
        }
    }
}
