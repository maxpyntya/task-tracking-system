﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class EmployeeAssociatedProject
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string CurrentEmployeePosition { get; set; }
        public double CompletionPercentage { get; set; }
    }
}
