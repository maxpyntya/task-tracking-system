﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class EmployeeWithRolePositionModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Position { get; set; }
        public string Role { get; set; }
    }
}
