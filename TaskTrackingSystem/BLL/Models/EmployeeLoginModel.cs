﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class EmployeeLoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }

    }
}
