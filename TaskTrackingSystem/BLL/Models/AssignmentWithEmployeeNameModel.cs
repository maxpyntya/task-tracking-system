﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class AssignmentWithEmployeeNameModel
    {
        public int AssignmentId { get; set; }
        public string AssignmentName { get; set; }
        public string AssignmentDescription { get; set; }
        public string AssignmentStatus { get; set; }
        public DateTime Deadline { get; set; }

        public int EmployeeId { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeLastName { get; set; }

        public int ProjectId { get; set; }

    }
}
