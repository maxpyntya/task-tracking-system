﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ProjectCreationModel
    {
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public double PercentageOfCompletion { get; set; }
        public DateTime StartDate { get; set; }

    }
}
