﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class PositionModel
    {
        public int PositionId { get; set; }
        public string PositionName { get; set; }
    }
}
