﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class EmployeeRegisterModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

    }
}
