﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ProjectWithTasksModel
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public double PercentageOfCompletion { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? CloseDate { get; set; }

        public IEnumerable<AssignmentModel> Assignments { get; set; }

    }
}
