﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using DAL.Interfaces;
using DAL.Models;
using NamedConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class AdministratorService : CommonServiceFeatures, IAdministratorService
    {
        public AdministratorService(IUnitOfWork UoW, IMapper mapper) : base(UoW, mapper)
        {
            _mapper = mapper;
        }

        public async Task AddProjectAsync(ProjectCreationModel model)
        {
            var project = _mapper.Map<Project>(model);
            await UnitOfWork.ProjectRepository.AddAsync(project);
            await UnitOfWork.SaveChangesAsync();
        }

        //cascade delete?
        public async Task DeleteProjectAsync(int projectId)
        {
            await UnitOfWork.ProjectRepository.DeleteByIdAsync(projectId);
            await UnitOfWork.SaveChangesAsync();
        }

        public async Task RemoveEmployeeFromRoleAsync(int employeeId, int projectId)
        {
            await UnitOfWork.JoinRepository.DeleteByIdAsync(employeeId, projectId);
            var assignments = UnitOfWork.AssignmentRepository.GetAll().Where(a => a.EmployeeId == employeeId && a.ProjectId == projectId);
            foreach(Assignment a in assignments)
            {
                a.EmployeeId = null;
                if (a.AssignmentStatus != AssignmentStatus.IsDone)
                    a.AssignmentStatus = AssignmentStatus.NoEmployee;
                UnitOfWork.AssignmentRepository.UpdateParticularField(a, nameof(a.EmployeeId));
            }
            await UnitOfWork.SaveChangesAsync();
        }

        public async Task SetEmployeeRoleAsync(int employeeId, int projectId, int roleId, int? positionId = null)
        {
            if (positionId == null)
            {
                var role = await UnitOfWork.RoleRepository.GetByIdAsync(roleId);
                positionId = UnitOfWork.PositionRepository.GetAll().FirstOrDefault(p => p.PositionName == role.Name).PositionId;
            }
            var joinRecord = UnitOfWork.JoinRepository.GetAll().FirstOrDefault(p => p.EmployeeId == employeeId && p.ProjectId == projectId);
            joinRecord.RoleId = roleId;
            joinRecord.PositionId = (int)positionId;
            UnitOfWork.JoinRepository.Update(joinRecord);
            await UnitOfWork.SaveChangesAsync();
        }

        public IEnumerable<PositionModel> ShowAvailablePositionsByName(int projectId, string positionToFind)
        {
            var specialPositionsList = SpecialPositions.GetSpecialPositions();
            var availablePositions = UnitOfWork.PositionRepository.GetAll().Where(p => !specialPositionsList.Contains(p.PositionName));
            var matchedPositions = availablePositions.Where(p => p.PositionName.Contains(positionToFind));

            var positionModels = matchedPositions.Select(p => _mapper.Map<PositionModel>(p)).ToList();
            return positionModels;
        }

        public IEnumerable<RoleModel> ShowAvailableRoles(int projectId)
        {
            //select all roles used in projects, excluding Employee role
            var usedRolesIds = UnitOfWork.JoinRepository.GetAll().Where(j => j.ProjectId == projectId && j.RoleId!=1).Select(j=>j.RoleId).Distinct();
            var availableRoles = UnitOfWork.RoleRepository.GetAll().Where(r => !usedRolesIds.Any(rl => rl == r.Id)&&r.Name!="Admin");

            var roleModels = availableRoles.Select(r => _mapper.Map<RoleModel>(r)).ToList();
            return roleModels;
        }

        public IEnumerable<EmployeeModel> FindEmployeesByNameOnProject(int projectId, string firstName, string lastName)
        {
            var employees = UnitOfWork.EmployeeRepository.FindEmployeesByName(firstName, lastName);
            var empsOnProjects = UnitOfWork.JoinRepository.GetAll().Select(j => j.EmployeeId);
            var result = employees.Where(e => empsOnProjects.Contains(e.Id)).ToList();

            return result.Select(r => _mapper.Map<EmployeeModel>(r));
        }

        public IEnumerable<EmployeeWithRolePositionModel> FindEmployeesWithPositionsAndRolesByNameOnProject(int projectId, string firstName, string lastName)
        {
            var employees = UnitOfWork.JoinRepository.GetAllWithDetails()
                .Where(p => p.ProjectId == projectId && p.Employee.FirstName.Contains(firstName)&&p.Employee.LastName.Contains(lastName));
            var empModels = employees.Select(e => new EmployeeWithRolePositionModel
            {
                Id = e.EmployeeId,
                FirstName = e.Employee.FirstName,
                LastName = e.Employee.LastName,
                Email = e.Employee.Email,
                UserName = e.Employee.UserName,
                Position = e.Position.PositionName,
                Role = e.Role.Name
            }).ToList();

            return empModels;
        }


        public override void GetSpecificInformationToShowProjects(out IQueryable<Project> projects, out IQueryable<EmployeeProjectPositionRole> positions, int pageNumber, int numberOfProjects)
        {
            projects = UnitOfWork.ProjectRepository.GetAllWithDetails();
            positions = null;
        }

        public override IEnumerable<EmployeeAssociatedProject> ShowProjects(int numberOfProjects, int pageNumber = 0)
        {
            pageNumber = pageNumber == 0 ? 0 : pageNumber - 1;

            GetSpecificInformationToShowProjects(out IQueryable<Project> projects, out IQueryable<EmployeeProjectPositionRole> positions, pageNumber, numberOfProjects);

            //Use automapper here also, not going to be just here
            var associatedProjects = projects.Select(p => new EmployeeAssociatedProject
            {
                ProjectId = p.ProjectId,
                ProjectName = p.ProjectName,
                ProjectDescription = p.ProjectDescription,
                StartDate = p.StartDate,
                CloseDate = p.CloseDate,
                CurrentEmployeePosition = "Administrator",
                CompletionPercentage = p.PercentageOfCompletion
            }).ToList();

            return associatedProjects;
        }

    }
}
