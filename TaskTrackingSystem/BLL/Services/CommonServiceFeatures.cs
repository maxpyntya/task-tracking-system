﻿using AutoMapper;
using BLL.Models;
using DAL.Interfaces;
using DAL.Models;
using NamedConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public abstract class CommonServiceFeatures : IDisposable
    {
        private bool disposedValue;

        protected IUnitOfWork UnitOfWork { get; set; }
        protected IMapper _mapper;
        public int UserId { get; set; }

        public CommonServiceFeatures(IUnitOfWork UoW, IMapper mapper)
        {
            UnitOfWork = UoW;
            _mapper = mapper;
        }

        public virtual IEnumerable<EmployeeAssociatedProject> ShowProjects(int numberOfProjects, int pageNumber=0)
        {
            pageNumber = pageNumber == 0 ? 0 : pageNumber - 1;

            GetSpecificInformationToShowProjects(out IQueryable<Project> projects, out IQueryable<EmployeeProjectPositionRole> positions, pageNumber, numberOfProjects);

            //Use automapper here also, not going to be just here
            var associatedProjects = projects.Select(p => new EmployeeAssociatedProject
            {
                ProjectId = p.ProjectId,
                ProjectName = p.ProjectName,
                ProjectDescription = p.ProjectDescription,
                StartDate = p.StartDate,
                CloseDate = p.CloseDate,
                CurrentEmployeePosition = positions.Where(pos => pos.ProjectId == p.ProjectId && pos.EmployeeId == UserId).
                Select(pos => pos.Position).FirstOrDefault().PositionName,
                CompletionPercentage = p.PercentageOfCompletion
            }).ToList();

            return associatedProjects;

        }

        public virtual void GetSpecificInformationToShowProjects(out IQueryable<Project> projects, out IQueryable<EmployeeProjectPositionRole> positions, int pageNumber, int numberOfProjects)
        {
            projects = UnitOfWork.JoinRepository.GetAllWithDetails()/*.Where(p => p.EmployeeId == UserId)*/
                                .Select(p => p.Project).OrderBy(p=>p.ProjectId).Skip(pageNumber*numberOfProjects).Take(numberOfProjects);
            positions = UnitOfWork.JoinRepository.GetAll().Where(p => p.EmployeeId == UserId);
        }

        public virtual async Task<ProjectWithTasksModel> GetProjectAsync(int projectId, int numberOfTasksPerPage, int pageNumber = 0)
        {
            pageNumber = pageNumber == 0 ? 0 : pageNumber - 1;
            var project = await UnitOfWork.ProjectRepository.GetByIdWithDetailsAsync(projectId);
            var assignments = project.Assignments.OrderBy(a => a.AssignmentId).Skip(numberOfTasksPerPage * pageNumber).Take(numberOfTasksPerPage);
            project.Assignments = assignments.ToList();
            var projectModel = _mapper.Map<ProjectWithTasksModel>(project);
            return projectModel;
        }

        public virtual async Task UpdateProjectCompletionPercentage(int projectId)
        {
            var project = await UnitOfWork.ProjectRepository.GetByIdWithDetailsAsync(projectId);

            var projectTasks = project.Assignments.Select(a => a);

            double percentage = (double)(projectTasks.Where(a => a.AssignmentStatus == AssignmentStatus.IsDone).Count() * 100) / (double)projectTasks.Count();
            percentage = Math.Round(percentage, 2);
            project.PercentageOfCompletion = percentage;
            UnitOfWork.ProjectRepository.UpdateParticularField(project, nameof(project.PercentageOfCompletion));
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    UnitOfWork.Dispose();
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~CommonServiceFeatures()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
