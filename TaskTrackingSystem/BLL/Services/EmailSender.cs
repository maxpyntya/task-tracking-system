﻿using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using MailKit;
using MimeKit;
using Microsoft.Extensions.Configuration;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using DAL.Interfaces;

namespace BLL.Services
{
    public class EmailSender : IEmailSender
    {
        private bool disposedValue;
        IConfiguration _configuration;
        string systemEmail;
        string password;

        public IUnitOfWork UnitOfWork { get ; set ; }

        public EmailSender(IConfiguration config)
        {
            _configuration = config;
            systemEmail = _configuration.GetValue<string>("Mail");
            password = _configuration.GetValue<string>("Password");
        }
        public async Task SendEmail_AssignedToProjectAsync(int projectId, int userId)
        {
            var info = await UnitOfWork.JoinRepository.GetByIdWithDetailsAsync(userId, projectId);

            var message = new MimeMessage();

            message.From.Add(new MailboxAddress("Task Tracking System", systemEmail));
            message.To.Add(new MailboxAddress(info.Employee.FirstName+" "+ info.Employee.LastName, info.Employee.Email));
            message.Subject = "You were assigned to new project!";
            message.Body = new TextPart("Plain")
            {
                Text = $"Hello, {info.Employee.FirstName} {info.Employee.LastName}.\nYou have received this email because you were assigned on project {info.Project.ProjectName} as {info.Position.PositionName}.\n" +
                $"For more details, enter the system."
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 587);
                await client.AuthenticateAsync(systemEmail, password);
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
            }

        }
        

        public async Task SendEmail_TaskIssuedAsync(int projectId, int userId)
        {
            var info = await UnitOfWork.JoinRepository.GetByIdWithDetailsAsync(userId, projectId);

            var message = new MimeMessage();

            message.From.Add(new MailboxAddress("Task Tracking System", systemEmail));
            message.To.Add(new MailboxAddress(info.Employee.FirstName+ " " + info.Employee.LastName, info.Employee.Email));
            message.Subject = "You were issued a new task!";
            message.Body = new TextPart("Plain")
            {
                Text = $"Hello, {info.Employee.FirstName} {info.Employee.LastName}.\nYou have received this email because you were issued a new task on project {info.Project.ProjectName}.\n" +
                $"For more details, enter the system."
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 587);
                await client.AuthenticateAsync(systemEmail, password);
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
            }

        }
        public async Task SendEmail_PositionChangedAsync(int projectId, int userId)
        {
            var info = await UnitOfWork.JoinRepository.GetByIdWithDetailsAsync(userId, projectId);

            var message = new MimeMessage();

            message.From.Add(new MailboxAddress("Task Tracking System", systemEmail));
            message.To.Add(new MailboxAddress(info.Employee.FirstName + " " + info.Employee.LastName, info.Employee.Email));
            message.Subject = $"Your position was changed on project {info.Project.ProjectName}";
            message.Body = new TextPart("Plain")
            {
                Text = $"Hello, {info.Employee.FirstName} {info.Employee.LastName}.\nYou have received this email because your position has changed on project {info.Project.ProjectName}.\n" +
                $"Now, your current position is {info.Position.PositionName}" +
                $"For more details, enter the system."
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 587);
                await client.AuthenticateAsync(systemEmail, password);
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
            }
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    UnitOfWork.Dispose();
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~EmailSender()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

    }
}
