﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using DAL.Interfaces;
using DAL.Models;
using NamedConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class EmployeeService : CommonServiceFeatures, IEmployeeService
    {
        private bool disposedValue;
        public EmployeeService(IUnitOfWork UoW, IMapper mapper) : base(UoW, mapper)
        {
        }

        public override async Task<ProjectWithTasksModel> GetProjectAsync(int projectId, int numberOfTasksPerPage, int pageNumber = 0)
        {
            pageNumber = pageNumber == 0 ? 0 : pageNumber - 1;
            var proj = await UnitOfWork.ProjectRepository.GetByIdWithDetailsAsync(projectId);
            var assignments = proj.Assignments.Where(a => a.EmployeeId == UserId).OrderBy(a=>a.AssignmentId)
                .Skip(numberOfTasksPerPage*pageNumber).Take(numberOfTasksPerPage).ToList();
            proj.Assignments = assignments;
            var projectModel = _mapper.Map<ProjectWithTasksModel>(proj);
            return projectModel;
        }
        public override void GetSpecificInformationToShowProjects(out IQueryable<Project> projects, out IQueryable<EmployeeProjectPositionRole> positions, int pageNumber, int numberOfProjects)
        {
            projects = UnitOfWork.JoinRepository.GetAllWithDetails().Where(p => p.EmployeeId == UserId)
                                .Select(p => p.Project).OrderBy(p => p.ProjectId).Skip(pageNumber * numberOfProjects).Take(numberOfProjects);
            positions = UnitOfWork.JoinRepository.GetAll().Where(p => p.EmployeeId == UserId);
        }

        public async Task SetTaskStatusAsync(int taskId, string newStatus)
        {
            var task = await UnitOfWork.AssignmentRepository.GetByIdAsync(taskId);

            string oldStatus = task.AssignmentStatus;

            task.AssignmentStatus = newStatus;
            UnitOfWork.AssignmentRepository.UpdateParticularField(task, nameof(task.AssignmentStatus));
            await UnitOfWork.SaveChangesAsync();

            if (newStatus == AssignmentStatus.IsDone || oldStatus == AssignmentStatus.IsDone)
            {
                int projectId = task.ProjectId;
                await UpdateProjectCompletionPercentage(projectId);

                await UnitOfWork.SaveChangesAsync();
            }
        }

        //protected virtual void Dispose(bool disposing)
        //{
        //    if (!disposedValue)
        //    {
        //        if (disposing)
        //        {
        //            // TODO: dispose managed state (managed objects)
        //        }

        //        // TODO: free unmanaged resources (unmanaged objects) and override finalizer
        //        // TODO: set large fields to null
        //        disposedValue = true;
        //    }
        //}

        //// // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        //// ~EmployeeService()
        //// {
        ////     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        ////     Dispose(disposing: false);
        //// }

        //public void Dispose()
        //{
        //    // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //    Dispose(disposing: true);
        //    GC.SuppressFinalize(this);
        //}
    }
}
