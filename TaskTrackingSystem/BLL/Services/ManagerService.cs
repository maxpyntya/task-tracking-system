﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using DAL.Interfaces;
using DAL.Models;
using NamedConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ManagerService : CommonServiceFeatures, IManagerService
    {
        IEmailSender _emailSender;

        public ManagerService(IUnitOfWork UoW, IMapper mapper, IEmailSender sender) : base(UoW, mapper)
        {
            _mapper = mapper;
            _emailSender = sender;
            _emailSender.UnitOfWork = UoW;
        }

        public override async Task<ProjectWithTasksModel> GetProjectAsync(int projectId, int numberOfTasksPerPage, int pageNumber = 0)
        {
            pageNumber = pageNumber == 0 ? 0 : pageNumber - 1;
            var project = await UnitOfWork.ProjectRepository.GetByIdWithDetailsAsync(projectId);
            var assignments = project.Assignments.OrderBy(a=>a.AssignmentId).Skip(numberOfTasksPerPage * pageNumber).Take(numberOfTasksPerPage);
            project.Assignments = assignments.ToList();// as ICollection<Assignment>;
            var projectModel = _mapper.Map<ProjectWithTasksModel>(project);
            return projectModel;
        }
        //действительно ли этот метод нужен? Ведь по сути, в некоторых ролях можно обойтись только проектами, т.к.
        // уже заранее известно какая у сотрудника там позиция (менеджер тому пример)
        public override void GetSpecificInformationToShowProjects(out IQueryable<Project> projects, out IQueryable<EmployeeProjectPositionRole> positions, int pageNumber, int numberOfProjects)
        {
            projects = UnitOfWork.JoinRepository.GetAllWithDetails()/*.Where(p => p.EmployeeId == UserId)*/
                    .Select(p => p.Project).OrderBy(p => p.ProjectId).Skip(pageNumber * numberOfProjects).Take(numberOfProjects);
            positions = UnitOfWork.JoinRepository.GetAll().Where(p => p.EmployeeId == UserId&&p.Role.Name=="Manager");
            
        }

        public override IEnumerable<EmployeeAssociatedProject> ShowProjects(int numberOfProjects, int pageNumber = 0)
        {
            pageNumber = pageNumber == 0 ? 0 : pageNumber - 1;

            var projects = UnitOfWork.JoinRepository.GetAllWithDetails().Where(p => p.EmployeeId == UserId&&p.Role.Name==SpecialPositions.Manager)
                .Select(p => p.Project).OrderBy(p => p.ProjectId).Skip(pageNumber * numberOfProjects).Take(numberOfProjects);

            //Use automapper here also, not going to be just here
            var associatedProjects = projects.Select(p => new EmployeeAssociatedProject
            {
                ProjectId = p.ProjectId,
                ProjectName = p.ProjectName,
                ProjectDescription = p.ProjectDescription,
                StartDate = p.StartDate,
                CloseDate = p.CloseDate,
                CurrentEmployeePosition = SpecialPositions.Manager,
                CompletionPercentage = p.PercentageOfCompletion
            }).ToList();

            return associatedProjects;

        }

        public IEnumerable<EmployeeModel> FindEmployeesByNameToAssignProject(int projectId, string firstName, string lastName)
        {
            var employees = UnitOfWork.EmployeeRepository.FindEmployeesByName(firstName, lastName);
            var empsOnProject = UnitOfWork.JoinRepository.GetAll().Where(p => p.ProjectId == projectId).Select(p => p.EmployeeId);
            var result = employees.Where(e => !empsOnProject.Contains(e.Id)).ToList();

            var empModels = result.Select(e => _mapper.Map<EmployeeModel>(e));
            return empModels;
        }

        public IEnumerable<EmployeeModel> FindEmployeesByNameOnProject(int projectId, string firstName, string lastName)
        {
            var employees = UnitOfWork.EmployeeRepository.FindEmployeesByName(firstName, lastName);
            var empsOnProjects = UnitOfWork.JoinRepository.GetAll().Select(j=>j.EmployeeId);
            var result = employees.Where(e => empsOnProjects.Contains(e.Id)).ToList();

            return result.Select(r => _mapper.Map<EmployeeModel>(r));
        }
                                                                 
        public async Task IssueNewTaskToEmployeeAsync(int employeeId, AssignmentModel taskModel)
        {
            //The idea is that employees associated with project for task issuence
            //will be shown when entering their name in textbox, so they are not needed
            //in project model
            taskModel.AssignmentStatus = AssignmentStatus.IsNew;
            var task = _mapper.Map<Assignment>(taskModel);
            //  ProjectId to which the task is bound must be issued on client side!
            task.EmployeeId = employeeId;
            await UnitOfWork.AssignmentRepository.AddAsync(task);
            await UnitOfWork.SaveChangesAsync();

            await UpdateProjectCompletionPercentage(task.ProjectId);
            await UnitOfWork.SaveChangesAsync();

            // UNCOMMENT LATER
            //await _emailSender.SendEmail_TaskIssuedAsync(task.ProjectId, employeeId);
        }
        public async Task IssueExistingTaskToEmployeeAsync(int employeeId, int assignmentId)
        {
            var assignment = await UnitOfWork.AssignmentRepository.GetByIdAsync(assignmentId);
            assignment.EmployeeId = employeeId;
            UnitOfWork.AssignmentRepository.UpdateParticularField(assignment, nameof(assignment.EmployeeId));
            await UnitOfWork.SaveChangesAsync();

            // UNCOMMENT LATER
            //await _emailSender.SendEmail_TaskIssuedAsync(assignment.ProjectId, employeeId);
        }

        // (Опциональный метод) если обновляется дедлайн, то должен по идее обновляться
        // статус задания, и вместе с этим обновляется процент завершенности
        public async Task ProlongTaskDeadlineAsync(int taskId, string newDeadline)
        {
            var task = await UnitOfWork.AssignmentRepository.GetByIdAsync(taskId);
            task.Deadline = DateTime.Parse(newDeadline);
            UnitOfWork.AssignmentRepository.UpdateParticularField(task, nameof(task.Deadline));
            await UnitOfWork.SaveChangesAsync();
        }

        public async Task AssignEmployeeToProjectAsync(int projectId, int userId, int positionId)
        {
            await UnitOfWork.JoinRepository.AddAsync(new EmployeeProjectPositionRole
            {
                EmployeeId = userId,
                ProjectId = projectId,
                PositionId = positionId,
                RoleId = 1
            });
            await UnitOfWork.SaveChangesAsync();
            await _emailSender.SendEmail_AssignedToProjectAsync(projectId, userId);
        }

        public IEnumerable<PositionModel> FindPositionsByName(string positionName)
        {
            var specialPositions = SpecialPositions.GetSpecialPositions();
            var allPositionsThatContainName = UnitOfWork.PositionRepository.GetAll().Where(p => p.PositionName.Contains(positionName) /*&& !specialPositions.Any(p=>p.Contains(positionName))*/);
            var positionsThatContainNameWithoutSpecialPositions = UnitOfWork.PositionRepository.GetAll().Where(p=>!specialPositions.Contains(p.PositionName))/*.Where(p => !specialPositions.Any(sp => sp.Contains(positionName)))*/;
            var result = allPositionsThatContainName.Where(p => positionsThatContainNameWithoutSpecialPositions.Contains(p)).ToList();
            var positionModels = result.Select(p => _mapper.Map<PositionModel>(p));
            return positionModels;
        }

        public async Task ChangeEmployeePositionAsync(int employeeId, int projectId, int positionId)
        {
            var record = await UnitOfWork.JoinRepository.GetByIdAsync(employeeId, projectId);
            record.PositionId = positionId;
            UnitOfWork.JoinRepository.UpdateParticularField(record, nameof(record.PositionId));
            await UnitOfWork.SaveChangesAsync();

            // UNCOMMENT LATER
            //await _emailSender.SendEmail_PositionChangedAsync(projectId, employeeId);
        }
    }
}
