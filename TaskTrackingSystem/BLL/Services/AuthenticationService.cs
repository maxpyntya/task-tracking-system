﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using DAL.AccountManagement;
using DAL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NamedConstants;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        //Логика простая - способы валидации разные, поэтому они имплементируют интерфейс IAuthentication для функционала
        //регистрации/логина. Сами они будут находится внутри классов, кототрые имплементируют IAuthenticationService,
        //таким образом, оставляя контроллер в полном неведении того, что происходит внутри, а значит есть возможность легко заменять
        //сервисы, ничего не ломая в контроллере
        IAuthentication<Employee> _authAgent;
        IMapper _mapper;
        private bool disposedValue;

        public AuthenticationService(IAuthentication<Employee> authAgent, IMapper mapper)
        {
            _authAgent = authAgent;
            _mapper = mapper;
        }

        public async Task<UserAccountOperationResult> AddUserAsync(EmployeeRegisterModel model)
        {
            var employee = _mapper.Map<Employee>(model);

            var result = await _authAgent.CreateUserAsync(employee, model.Password);

            return result;
        }

        public async Task<UserAccountOperationResult> LoginUserAsync(EmployeeLoginModel model)
        {
            var employee = await _authAgent.FindByUsernameAsync(model.Username);
            if (employee == null)
                return new UserAccountOperationResult(OperationResultEnum.Failed, $"User with username {model.Username} doesn't exist in the system");

            var result = await _authAgent.PasswordSignInAsync(employee, model.Password);
            
            return result;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _authAgent.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~AuthenticationService()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
