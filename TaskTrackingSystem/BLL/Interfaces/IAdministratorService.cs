﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IAdministratorService : IUserService
    {
        Task AddProjectAsync(ProjectCreationModel model);
        Task DeleteProjectAsync(int projectId);
        Task RemoveEmployeeFromRoleAsync(int employeeId, int projectId);
        Task SetEmployeeRoleAsync(int employeeId, int projectId,  int roleId, int? positionId);
        IEnumerable<RoleModel> ShowAvailableRoles(int projectId);
        IEnumerable<PositionModel> ShowAvailablePositionsByName(int projectId, string positionToFind);
        //Common in both administrator and manager.
        IEnumerable<EmployeeWithRolePositionModel> FindEmployeesWithPositionsAndRolesByNameOnProject(int projectId, string firstName, string lastName);

    }
}
