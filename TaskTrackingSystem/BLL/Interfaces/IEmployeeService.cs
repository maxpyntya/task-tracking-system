﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IEmployeeService : IUserService
    {
        Task SetTaskStatusAsync(int taskId, string newStatus);
    }
}
