﻿using BLL.Models;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IManagerService : IUserService
    {
        Task IssueNewTaskToEmployeeAsync(int employeeId, AssignmentModel task);
        Task IssueExistingTaskToEmployeeAsync(int employeeId, int assignmentId);
        Task ProlongTaskDeadlineAsync(int taskId, string newDeadline);
        Task ChangeEmployeePositionAsync(int employeeId, int projectId, int positionId);
        IEnumerable<EmployeeModel> FindEmployeesByNameToAssignProject(int projectId,string firstName, string lastName);
        Task AssignEmployeeToProjectAsync(int projectId, int userId, int positionId);
        IEnumerable<PositionModel> FindPositionsByName(string positionName);
        public IEnumerable<EmployeeModel> FindEmployeesByNameOnProject(int projectId, string firstName, string lastName);

    }
}
