﻿using BLL.Models;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NamedConstants;


namespace BLL.Interfaces
{
    public interface IAuthenticationService : IDisposable//, IAuthentication<Employee>
    {
        Task<UserAccountOperationResult> AddUserAsync(EmployeeRegisterModel model);
        Task<UserAccountOperationResult> LoginUserAsync(EmployeeLoginModel model);
    }
}
