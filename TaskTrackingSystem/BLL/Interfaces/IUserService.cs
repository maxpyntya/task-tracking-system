﻿using BLL.Models;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserService : IDisposable
    {
        public abstract int UserId { get; set; }
        IEnumerable<EmployeeAssociatedProject> ShowProjects(int numberOfProjects, int pageNumber = 0);        
        void GetSpecificInformationToShowProjects(out IQueryable<Project> projects, out IQueryable<EmployeeProjectPositionRole> positions, int pageNumber, int numberOfProjects);

        Task<ProjectWithTasksModel> GetProjectAsync(int projectId, int numberOfTasksPerPage, int pageNumber=0);
        Task UpdateProjectCompletionPercentage(int projectId);
    }
}
