﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IEmailSender : IDisposable
    {
        IUnitOfWork UnitOfWork { get; set; }

        Task SendEmail_TaskIssuedAsync(int projectId, int userId);
        Task SendEmail_PositionChangedAsync(int projectId, int userId);
        Task SendEmail_AssignedToProjectAsync(int projectId, int userId);

    }
}
