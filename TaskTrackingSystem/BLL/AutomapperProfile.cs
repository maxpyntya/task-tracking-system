﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using BLL.Models;
using DAL.Models;

namespace BLL
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Project, ProjectWithTasksModel>().ReverseMap();

            CreateMap<Project, ProjectCreationModel>().ReverseMap();

            CreateMap<Assignment, AssignmentModel>().ReverseMap();

            CreateMap<Assignment, AssignmentWithEmployeeNameModel>()
                .ForMember(dest => dest.AssignmentId, opts => opts.MapFrom(source => source.AssignmentId))
                .ForMember(dest => dest.AssignmentName, opts => opts.MapFrom(source => source.AssignmentName))
                .ForMember(dest => dest.AssignmentDescription, opts => opts.MapFrom(source => source.AssignmentDescription))
                .ForMember(dest => dest.AssignmentStatus, opts => opts.MapFrom(source => source.AssignmentStatus))
                .ForMember(dest => dest.Deadline, opts => opts.MapFrom(source => source.Deadline))
                .ForMember(dest => dest.EmployeeId, opts => opts.MapFrom(source => source.EmployeeId))
                .ForMember(dest => dest.EmployeeFirstName, opts => opts.MapFrom(source => source.Employee.FirstName))
                .ForMember(dest => dest.EmployeeLastName, opts => opts.MapFrom(source => source.Employee.LastName))
                .ReverseMap();

            CreateMap<Employee, EmployeeModel>().ReverseMap();

            CreateMap<Employee, EmployeeLoginModel>().ReverseMap();

            CreateMap<Employee, EmployeeRegisterModel>().ReverseMap();

            CreateMap<Position, PositionModel>().ReverseMap();

            CreateMap<Role, RoleModel>().ReverseMap();
        }
    }
}
